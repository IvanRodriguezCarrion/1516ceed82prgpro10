package org.ceed.ceed82prgpro10.modelo;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Usuario extends Persona {
 /**
 * Variables de clase (que verán todas las funciones del ModeloPadrino
 * y corresponden a los atributos de la tabla USUARIO
 */
  private final String usuario = "admin";
  private final String password = "admin";

  public String getUsuario () {
    return usuario;
  }
    public String getPassword () {
    return password;
  }
    
  
  

}