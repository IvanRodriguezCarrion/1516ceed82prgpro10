package org.ceed.ceed82prgpro10.modelo;
// Generated 29-abr-2016 20:52:15 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Perros generated by hbm2java
 */
public class Perros  implements java.io.Serializable {


     private Integer idperro;
     private String nombreperro;
     private Long nchip;
     private String raza;
     private Set apadrinas = new HashSet(0);

    public Perros() {
    }

    public Perros(String nombreperro, Long nchip, String raza, Set apadrinas) {
       this.nombreperro = nombreperro;
       this.nchip = nchip;
       this.raza = raza;
       this.apadrinas = apadrinas;
    }
    
    public Perros(int idperro, String nombreperro, Long nchip, String raza) {
       this.idperro = idperro;
	this.nombreperro = nombreperro;
       this.nchip = nchip;
       this.raza = raza;
    }
   
    public Integer getIdperro() {
        return this.idperro;
    }
    
    public void setIdperro(Integer idperro) {
        this.idperro = idperro;
    }
    public String getNombreperro() {
        return this.nombreperro;
    }
    
    public void setNombreperro(String nombreperro) {
        this.nombreperro = nombreperro;
    }
    public Long getNchip() {
        return this.nchip;
    }
    
    public void setNchip(Long nchip) {
        this.nchip = nchip;
    }
    public String getRaza() {
        return this.raza;
    }
    
    public void setRaza(String raza) {
        this.raza = raza;
    }
    public Set getApadrinas() {
        return this.apadrinas;
    }
    
    public void setApadrinas(Set apadrinas) {
        this.apadrinas = apadrinas;
    }




}


