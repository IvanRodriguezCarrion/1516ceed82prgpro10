package org.ceed.ceed82prgpro10.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Date;
import org.ceed.ceed82prgpro10.vista.Funciones;


public class ModeloFichero implements IModelo {
    private static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private File archivo_padrino = new File("padrino.csv");
    private File archivo_perro = new File("perro.csv");
    private File archivo_apadrinamiento = new File("apadrinamiento.csv");
    private Funciones f = new Funciones();
    private FileWriter fw;
    
    public ModeloFichero() {
	if (!archivo_padrino.exists() || !archivo_perro.exists() || !archivo_apadrinamiento.exists()) {
			Funciones.mensaje(null, "No existe la base de datos CSV, debes insalarla.", "ERROR EN LA INSTALACION DEL CSV", 1);
		}
	
    }

    @Override
    public void create(Padrinos p) {
	p.setIdpersona(idPadrino());
        try {
		FileWriter fw = new FileWriter(archivo_padrino, true);
		fw.write(p.getIdpersona() + ";" + p.getNombrepersona() + ";"+ p.getTelefono() + ";" + p.getEmail() + ";\r\n");
		fw.close();
		f.mensaje(null, "Padrino creado con éxito", "CREACIÓN PADRINO", 1);
           } catch (IOException ex) {
		Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
		f.mensaje(null, "Fallo en la creación del padrino. \nError: " + ex.getMessage(), "CREACIÓN PADRINO", 0);
        }
    }
    
    @Override
    public void create(Perros perro) {
	perro.setIdperro(idPerro());
        try {
            FileWriter fw = new FileWriter(archivo_perro, true);
            fw.write(perro.getIdperro() + ";" + perro.getNombreperro() + ";"
                    + perro.getNchip() + ";" + perro.getRaza() + ";\r\n");
            fw.close();
	    f.mensaje(null, "Perro creado con éxito", "CREACIÓN PERRO", 1);
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
	    f.mensaje(null, "Fallo en la creación del perro. \nError: " + ex.getMessage(), "CREACIÓN PERRO", 0);
        }  
    }
    
    @Override
    public void create(Apadrina apa) {
        apa.setIdapadrina(idApadrina());
        try {
            FileWriter fw = new FileWriter(archivo_apadrinamiento, true);                    
            fw.write(apa.getIdapadrina() + ";" + apa.getPadrinos().getIdpersona() 
                    + ";" + apa.getPerros().getIdperro() + ";" + SHORT_DATE_FORMAT.format(apa.getFechaapa())+ ";\r\n");
            fw.close();
	    f.mensaje(null, "Apadrinamiento creado con éxito", "CREACIÓN APADRINAMIENTO", 1);
           } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
	    f.mensaje(null, "Fallo en la creación del apadrinamiento. \nError: " + ex.getMessage(), "CREACIÓN APADRINAMIENTO", 0);
        }
    }
    
   public ArrayList<Padrinos> readPadrino() {   
        ArrayList hs = new ArrayList();
        try {
            FileReader fr = new FileReader(archivo_padrino);
            BufferedReader br = new BufferedReader(fr);
            String s;
            s = br.readLine();
            while (s != null) {
                Padrinos padrino = obtenerPadrino(s);
                hs.add(padrino);
                s = br.readLine();
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
	    f.mensaje(null, "Fallo en la lectura de los padrinos. \nError: " + e.getMessage(), "LEER PADRINO", 0);
        }
        return hs;
    }
 
    @Override
    public ArrayList<Perros> readPerro() {        
        ArrayList hs = new ArrayList();        
        try {
            FileReader fr = new FileReader(archivo_perro);
            BufferedReader br = new BufferedReader(fr);
            String s;
            s = br.readLine();
            while (s != null) {
                Perros perro = obtenerPerro(s);
                hs.add(perro);
                s = br.readLine();
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
	    f.mensaje(null, "Fallo en la lectura de los perros. \nError: " + e.getMessage(), "LEER PERROS", 0);
        }
        return hs;
    }
     
      @Override
     public ArrayList<Apadrina> readApadrina() { //Hace lo mismo que Padrino        
        ArrayList hs = new ArrayList();        
        try {
            FileReader filereader = new FileReader(archivo_apadrinamiento);
            BufferedReader br = new BufferedReader(filereader);
            String s;
            s = br.readLine();
            while (s != null) {   
                Apadrina apadrina = obtenerApadrina(s);
                hs.add(apadrina);
                s = br.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
	    f.mensaje(null, "Fallo en la lectura de los apadriamientos. \nError: " + e.getMessage(), "LEER APADRINAMIENTOS", 0);
        }
        return hs;
    }

    @Override
    public void update(Padrinos p) {
        File archivo_temporal = new File("temp.csv");        
        try {
            FileWriter filewriter = new FileWriter(archivo_temporal, true);
            FileReader filereader = new FileReader(archivo_padrino);
            BufferedReader bf = new BufferedReader(filereader);
            String s;
            s = bf.readLine();
            while (s != null) {
                Padrinos padrino = obtenerPadrino(s);
                if (p.getIdpersona() == padrino.getIdpersona()) {
                   filewriter.write(p.getIdpersona() + ";" + p.getNombrepersona() + ";"+ p.getTelefono() + ";" + p.getEmail() + ";\r\n");
                } else {
                    filewriter.write(padrino.getIdpersona() + ";" + padrino.getNombrepersona() + ";"+ padrino.getTelefono() + ";" + padrino.getEmail() + ";\r\n");
                }
                s = bf.readLine();
            }
            filewriter.close();
            filereader.close();
	    f.mensaje(null, "Padrino actualizado con éxito", "ACTUALIZACION PADRINO", 1);
        } catch (IOException ex) {
            ex.printStackTrace();
	    f.mensaje(null, "Fallo en la actualización del padrino. \nError: " + ex.getMessage(), "ACTUALIZAR PADRINO", 0);
        }
        archivo_padrino.delete();
        archivo_temporal.renameTo(new File("padrino.csv"));
    }
     
    @Override
    public void update(Perros p) {
        File fo = new File("temp.csv");        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_perro);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                Perros perro = obtenerPerro(s);
                if (p.getIdperro() == perro.getIdperro()) {
                   ft.write(p.getIdperro() + ";" + p.getNombreperro() + ";"+ p.getNchip() + ";" + p.getRaza() + ";\r\n");
                } else {
                    ft.write(perro.getIdperro() + ";" + perro.getNombreperro() + ";"+ perro.getNchip() + ";" + perro.getRaza() + ";\r\n");
                }
                s = bf.readLine();
            }
            ft.close();
            fr.close();
	    f.mensaje(null, "Perro actualizado con éxito", "ACTUALIZACION PERRO", 1);
        } catch (IOException ex) {
            ex.printStackTrace();
	    f.mensaje(null, "Fallo en la actualización del perro. \nError: " + ex.getMessage(), "ACTUALIZAR PERRO", 0);	    
        }
        archivo_perro.delete();
	fo.renameTo(new File("perro.csv"));
    } 
    
     @Override
    public void update(Apadrina apa) {
        File archivo_temporal = new File("temp.csv");        
        try {
            FileWriter filewriter = new FileWriter(archivo_temporal, true);
            FileReader filereader = new FileReader(archivo_apadrinamiento);
            BufferedReader bf = new BufferedReader(filereader);
            String s;
            s = bf.readLine();
            while (s != null) {
                Apadrina apadrina = obtenerApadrina(s);
                if (apa.getIdapadrina() == apadrina.getIdapadrina()) {
                    filewriter.write(apa.getIdapadrina() + ";" + apa.getPadrinos().getIdpersona() + ";" + apa.getPerros().getIdperro() +  ";" + SHORT_DATE_FORMAT.format(apa.getFechaapa()) + ";\r\n");
                } else {
                    filewriter.write(apadrina.getIdapadrina() + ";" + apadrina.getPadrinos().getIdpersona() + ";" + apadrina.getPerros().getIdperro() + ";" + SHORT_DATE_FORMAT.format(apadrina.getFechaapa()) +";\r\n");
                }
                s = bf.readLine();
            }
            filewriter.close();
            filereader.close();
	    f.mensaje(null, "Apadrinamiento actualizado con éxito", "ACTUALIZACION APADRINAMIENTO", 1);
        } catch (IOException ex) {
            ex.printStackTrace();
	    f.mensaje(null, "Fallo en la actualización del apadrinamiento. \nError: " + ex.getMessage(), "ACTUALIZAR APADRINAMIENTO", 0);
        }
        archivo_apadrinamiento.delete();
        archivo_temporal.renameTo(new File("apadrinamiento.csv"));
    }
    
    @Override
     public void delete(Padrinos p) {
        File fo = new File("temp.csv");        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_padrino);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                Padrinos padrino = obtenerPadrino(s);
                if (p.getIdpersona() !=padrino.getIdpersona()) {
                    ft.write(padrino.getIdpersona() + ";" + padrino.getNombrepersona() + ";"+ padrino.getTelefono() + ";" + padrino.getEmail() + ";\r\n");
                } 
                s = bf.readLine();
            }
            ft.close();
            fr.close();
	    f.mensaje(null, "Padrino eliminado con éxito", "BORRADO PADRINO", 1);
        } catch (IOException ex) {
            ex.printStackTrace();
	    f.mensaje(null, "Fallo en el borrado del padrino. \nError: " + ex.getMessage(), "BORRAR PADRINO", 0);
        }
        archivo_padrino.delete();
        fo.renameTo(new File("padrino.csv"));
    }
    
    @Override
    public void delete(Perros p) {
        File fo = new File("temp.csv");        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_perro);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                Perros perro = obtenerPerro(s);
                if (p.getIdperro() != perro.getIdperro()) {
                    ft.write(perro.getIdperro() + ";" + perro.getNombreperro() + ";"+ perro.getNchip() + ";" + perro.getRaza() + ";\r\n");
                } 
                s = bf.readLine();
            }
            ft.close();
            fr.close();
	    f.mensaje(null, "Perro eliminado con éxito", "BORRAR PERRO", 1);
        } catch (IOException ex) {
            ex.printStackTrace();
	    f.mensaje(null, "Fallo en el borrado del perro. \nError: " + ex.getMessage(), "BORRAR PERRO", 0);
        }
        archivo_perro.delete();
        fo.renameTo(new File("perro.csv"));
    }

    @Override
  public void delete(Apadrina p) {
        File fo = new File("temp.csv");        
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(archivo_apadrinamiento);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                Apadrina apa = obtenerApadrina(s);
                if (p.getIdapadrina() != apa.getIdapadrina()) {
                    ft.write(apa.getIdapadrina() + ";" + apa.getPadrinos().getIdpersona() + ";" + apa.getPerros().getIdperro() + ";" + SHORT_DATE_FORMAT.format(apa.getFechaapa()) + ";\r\n");
                } 
                s = bf.readLine();
            }
            ft.close();
            fr.close();
	    f.mensaje(null, "Apadrinamiento eliminado con éxito", "BORRAR APADRINAMIENTO", 1);
        } catch (IOException ex) {
            ex.printStackTrace();
	    f.mensaje(null, "Fallo en el borrado del apadrinamiento. \nError: " + ex.getMessage(), "BORRAR APADRINAMIENTO", 0);
        }
        archivo_apadrinamiento.delete();
        fo.renameTo(new File("apadrinamiento.csv"));
    }

    @Override
    public void instalarDb() {

        try {
	    if (!archivo_padrino.exists()) {
		    fw = new FileWriter(archivo_padrino);
		    fw.close();
	    }
	    if (!archivo_perro.exists()) {
		    fw = new FileWriter(archivo_perro);
		    fw.close();
	    }
	    if (!archivo_apadrinamiento.exists()) {
		    fw = new FileWriter(archivo_apadrinamiento);
		    fw.close();
	    }
    } catch (Exception e) {
	    e.printStackTrace();
	    Funciones.mensaje(null, "Ha fallado la creación de los archivos CSV. \nError: " + e.getMessage(), 
		    "ERRROR EN CREACION DE CSV", 1);
		}
    }

    @Override
    public void desinstalarDb() {
        archivo_apadrinamiento.delete();
        archivo_perro.delete();
        archivo_padrino.delete();
    }

     //Otras funciones
    private int idPadrino() {
        int idultima = 0;
        if (archivo_padrino.exists()) {
            try {
                FileReader fr = new FileReader(archivo_padrino);
                BufferedReader br = new BufferedReader(fr);

                String linea = br.readLine();
                while (linea != null) {
                    Padrinos padrino = obtenerPadrino(linea);
                    int id = padrino.getIdpersona();
                    if (id > idultima) {
                        idultima = id;
                    }
                    linea = br.readLine();
                }
            fr.close();
            } catch (FileNotFoundException ex) {
		f.mensaje(null, "Fallo al buscar la ID de Padrino. \nError: " + ex.getMessage(), "BUSQUEDA DE ID", 0);
            } catch (IOException ex) {
		f.mensaje(null, "Fallo al buscar la ID de Padrino. \nError: " + ex.getMessage(), "BUSQUEDA DE ID", 0);
            }
        }
        return idultima +1;
    }
    
    private int idPerro() {
        int idultima = 0;
        if (archivo_perro.exists()) {
            try {
                FileReader fr = new FileReader(archivo_perro);
                BufferedReader br = new BufferedReader(fr);

                String linea = br.readLine();
                while (linea != null) {
                    Perros perro = obtenerPerro(linea);
                    int id = perro.getIdperro();
                    if (id > idultima) {
                        idultima = id;
                    }
                    linea = br.readLine();
                }
            fr.close();
            } catch (FileNotFoundException ex) {
		f.mensaje(null, "Fallo al buscar la ID de Perro. \nError: " + ex.getMessage(), "BUSQUEDA DE ID", 0);
            } catch (IOException ex) {
		f.mensaje(null, "Fallo al buscar la ID de Perro. \nError: " + ex.getMessage(), "BUSQUEDA DE ID", 0);
            }
        }
        return idultima +1;
    }

    private int idApadrina() {
        int idultima = 0;
        if (archivo_apadrinamiento.exists()) {
            try {
                FileReader fr = new FileReader(archivo_apadrinamiento);
                BufferedReader br = new BufferedReader(fr);

                String linea = br.readLine();
                while (linea != null) {
                    Apadrina a = obtenerApadrina(linea);
                    int id = a.getIdapadrina();
                    if (id > idultima) {
                        idultima = id;
                    }
                    linea = br.readLine();
                }
            fr.close();
            } catch (FileNotFoundException ex) {
		f.mensaje(null, "Fallo al buscar la ID de Apadrinamiento. \nError: " + ex.getMessage(), "BUSQUEDA DE ID", 0);
            } catch (IOException ex) {
		f.mensaje(null, "Fallo al buscar la ID de Apadrinamiento. \nError: " + ex.getMessage(), "BUSQUEDA DE ID", 0);
            }
        }
        return idultima +1;
    }
    
    private Padrinos obtenerPadrino(String s) {
        StringTokenizer stokenizer = new StringTokenizer(s, ";");
        int idPadrino = Integer.parseInt(stokenizer.nextToken());
        String nombre = stokenizer.nextToken();
        String telefono = stokenizer.nextToken();
        String email = stokenizer.nextToken();
        Padrinos p = new Padrinos(idPadrino, nombre, telefono, email);
        return p;
    }
    
    private Perros obtenerPerro(String s) {
        StringTokenizer stokenizer = new StringTokenizer(s, ";");
        int idPerro = Integer.parseInt(stokenizer.nextToken());
        String nombre = stokenizer.nextToken();
        long nChip = Long.parseLong(stokenizer.nextToken());
        String raza = stokenizer.nextToken();
        Perros p = new Perros(idPerro, nombre, nChip,raza);
        return p;
    }
    
    private Apadrina obtenerApadrina(String s) {
        StringTokenizer stokenizer = new StringTokenizer(s, ";");
        int idApadrina = Integer.parseInt(stokenizer.nextToken());
        Date fecha = null;
        int idPadrino = Integer.parseInt(stokenizer.nextToken());
        int idPerro = Integer.parseInt(stokenizer.nextToken());
	try {
            fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
        } catch (ParseException pe) {
	    f.mensaje(null, "Fallo al buscar transformar la fecha. \nError: " + pe.getMessage(), "CONVERSION DE FECHA", 0);
        }
        Padrinos padrino = new Padrinos(idPadrino, "", "", "");
	long nchip = 0;
        Perros perro = new Perros(idPerro, "", nchip, "");
        Apadrina a = new Apadrina(idApadrina, padrino, perro, fecha);
        return a;
    }

    @Override
    public void conectarBd() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void desconectarBd() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}