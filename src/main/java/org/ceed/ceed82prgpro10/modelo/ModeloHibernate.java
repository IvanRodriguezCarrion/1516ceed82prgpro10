/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceed.ceed82prgpro10.modelo;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.PooledConnection;
import org.ceed.ceed82prgpro10.controlador.HibernateUtil;
import org.ceed.ceed82prgpro10.vista.Funciones;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.ArrayList;

/**
 *
 * @author BlackBana
 */
public class ModeloHibernate implements IModelo{
    private final String bbdd = "1516ceed82prg";
    private final String usuario = "alumno";
    private final String password = "alumno";
    private final String jdbcUrl = "jdbc:mysql://localhost/" + bbdd;
    private final String driver = "com.mysql.jdbc.Driver";
    private final MysqlConnectionPoolDataSource mcpds = new MysqlConnectionPoolDataSource();
    private final Funciones f = new Funciones();
    private ArrayList padrinos, perros, apadrinas;
    private PooledConnection pc = null;
    private java.sql.Connection cn = null;
    private Statement st = null;
    private ResultSet rs = null;
    private String error, sql;

    public ModeloHibernate() {
	    //Configuración del pool de conexiones
	    mcpds.setUser(usuario);
	    mcpds.setPassword(password);
	    mcpds.setUrl(jdbcUrl);
    }

    @Override
    public void conectarBd() {
	    try {
		    pc = mcpds.getPooledConnection();
		    cn = pc.getConnection();
		    System.out.println("Base de datos " + bbdd + ": conectada");
	    } catch (SQLException se) {
		    se.printStackTrace();
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    @Override
    public void desconectarBd() {
	    try {
		    System.out.println("Base de datos " + bbdd + ": desconectada");
		    cn.close();
	    } catch (SQLException se) {
		    se.printStackTrace();
	    }
    }

    @Override
    public void instalarDb() {
	    st = null;
	    error = null;
	    try {
		    Class.forName(driver).newInstance();
		    cn = ((java.sql.Connection) DriverManager.getConnection("jdbc:mysql://localhost/", usuario, password));
		    st = cn.createStatement();
		    sql = "CREATE DATABASE IF NOT EXISTS " + bbdd + ";";
		    System.out.println(sql);
		    st.executeUpdate(sql);
		    f.mensaje(null, "Base de datos creada con éxito", "CREACION DE LA BASE DE DATOS",1);
	    } catch (Exception e) {
		    error = e.getMessage();
		    Funciones.mensaje(null, "Fallo en la instalación de la base de datos. \nError: " + e.getMessage(), "CREACION DE LA BASE DE DATOS",0);
	    } finally {
		    if (st != null) {
			    try {
				    st.close();
			    } catch (SQLException e) {
				    error = e.getMessage();
			    }
		    }
		    if (cn != null) {
			    try {
				    cn.close();
			    } catch (SQLException se) {
				    error = se.getMessage();
			    }
		    }
	    }
	    if (error != null) {
		    f.mensaje(null, "Fallo en la instalación de la base de datos. \nError: " + error, "CREACION DE LA BASE DE DATOS",0);
	    }
	    crearTablas();
	    crearDatos();
    }

    public void crearTablas() {
	    error = null;
	    try {
		    conectarBd();
            st = (Statement) cn.createStatement();
            
            sql = "DROP TABLE IF EXISTS apadrina;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "DROP TABLE IF EXISTS padrinos;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS perros;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "CREATE TABLE IF NOT EXISTS `padrinos` (\n"
               + "  `idpersona` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `nombrepersona` varchar(20),\n"
               + "  `telefono` varchar(15),\n"
               + "  `email` varchar(50),\n"
               + "  PRIMARY KEY (`idpersona`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);


            sql = "CREATE TABLE IF NOT EXISTS `perros` (\n"
               + "  `idperro` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `nombreperro` varchar(20),\n"
               + "  `nchip` BIGINT(15),\n"
               + "  `raza` varchar(15),\n"
               + "  PRIMARY KEY (`idperro`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);

            
            sql = "CREATE TABLE IF NOT EXISTS `apadrina` (\n"
               + "  `idapadrina` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `idpadrino_apa` int(5) NOT NULL,\n"
               + "  `idperro_apa` int(5) NOT NULL,\n"
               + "  `fechaapa` date NOT NULL,\n"
               + "  PRIMARY KEY (`idapadrina`),\n"    
               + "  FOREIGN KEY (`idpadrino_apa`) REFERENCES padrinos(idpersona),\n"
               + "  FOREIGN KEY (`idperro_apa`) REFERENCES perros(idperro)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            Funciones.mensaje(null, "Tablas creadas con éxito.", "CREACION DE TABLAS",1);
            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            Funciones.mensaje(null, "Fallo en la creación de las tablas. \nError: " + ex.getMessage(), "CREACION DE TABLAS",0);
        }
	    desconectarBd();
    }

    public void crearDatos() {
	    error = null;
	    try {
            conectarBd();
            st = (Statement) cn.createStatement();
            
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino1','1111111', 'padrino1@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
			
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino2','2222222', 'padrino2@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino3','33333333', 'padrino3@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro1', 123456789012345,'mestizo');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro2', 123456789012345,'Raza1');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro3', 123456789012345,'Raza2');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO apadrina (idpadrino_apa,idperro_apa,fechaapa) VALUES(1, 1,'2016/05/12');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO apadrina (idpadrino_apa,idperro_apa,fechaapa) VALUES(2, 2,'2016/01/17');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            Funciones.mensaje(null, "Datos genéricos insertados con éxito.", "INSERCION DE DATOS",1);
            
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            Funciones.mensaje(null, "Fallo en la inserción de datos genéricos. \nError: " + ex.getMessage(), "INSERCION DE DATOS",0);
        }
        desconectarBd();
    }

    @Override
    public void desinstalarDb() {
	   cn = null;
  	st = null;
  	error = null;
  	try {
            conectarBd();
            st = cn.createStatement();
            sql = "DROP DATABASE IF EXISTS " + bbdd + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
  	} catch (Exception e) {
            Funciones.mensaje(null, "Fallo en la desinstalación de la base de datos. \nError: " + e.getMessage(), "ELIMINACIÓN DE LA BASE DE DATOS",0);
  	} finally {
            if (st != null) {
                    try {
                    st.close();
                    } catch (SQLException e) {
                        Funciones.mensaje(null, "Fallo en la desinstalación de la base de datos. \nError: " + e.getMessage(), "ELIMINACIÓN DE LA BASE DE DATOS",0);
                    }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException se) {
                    Funciones.mensaje(null, "Fallo en la desinstalación de la base de datos. \nError: " + se.getMessage(), "ELIMINACIÓN DE LA BASE DE DATOS",0);
                }
            }
        }
    }

    @Override
    public void create(Padrinos padrino) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.save(padrino);
		    session.getTransaction().commit();
		    Funciones.mensaje(null, "Padrino creado con éxito.", "PADRINOS EN MYSQL",1);
	    } catch (HibernateException he) {
		    he.printStackTrace();
		    Funciones.mensaje(null, "Fallo al crear el padrino \nError: " + he.getMessage(), "CREAR PADRINOS EN MYSQL",0);
		    
	    }
    }

    @Override
    public void create(Perros perro) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.save(perro);
		    session.getTransaction().commit();
		    Funciones.mensaje(null, "Perro creado con éxito", "CREAR PERRO EN MYSQL",1);
	    } catch (HibernateException he) {
		    he.printStackTrace();
		    Funciones.mensaje(null, "Fallo al crear el perro. \nError: " + he.getMessage() , "CREAR PERRO EN MYSQL",0);
	    }
    }

    @Override
    public void create(Apadrina apadrina) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.save(apadrina);
		    session.getTransaction().commit();
		    Funciones.mensaje(null, "Apadrinamiento creado con éxito.", "CREAR APADRINAMIENTO EN MYSQL",1);
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
    }

    @Override
    public ArrayList<Padrinos> readPadrino(){
	    padrinos = new ArrayList();
	    try {
		    String hql = "from Padrinos";
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    Query q = session.createQuery(hql);
		    padrinos = (ArrayList) q.list();
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
	    return padrinos;
    }

    @Override
    public ArrayList readPerro() {
	    perros = new ArrayList();
	    try {
		    String hql = "from Perros";
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    Query q = session.createQuery(hql);
		    perros = (ArrayList) q.list();
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
	    return perros;
    }

    @Override
    public ArrayList readApadrina() {
		    apadrinas = new ArrayList();
	    try {
		    String hql = "from Apadrina";
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    Query q = session.createQuery(hql);
		    apadrinas = (ArrayList) q.list();
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
	    return apadrinas;
    }

    @Override
    public void update(Padrinos pa) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.update(pa);
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    f.mensaje(null,he.getMessage(),"ERROR",1);
		    he.printStackTrace();
	    }
    }

    @Override
    public void update(Perros pe) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.update(pe);
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
    }

    @Override
    public void update(Apadrina apa) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.update(apa);
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
    }

    @Override
    public void delete(Padrinos pa) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.delete(pa);
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
    }

    @Override
    public void delete(Perros pe) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.delete(pe);
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
    }

    @Override
    public void delete(Apadrina apa) {
	    try {
		    Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();
		    session.delete(apa);
		    session.getTransaction().commit();
	    } catch (HibernateException he) {
		    he.printStackTrace();
	    }
    }
}
