package org.ceed.ceed82prgpro10.modelo;

import java.sql.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.PooledConnection;
import java.text.SimpleDateFormat;
import org.ceed.ceed82prgpro10.vista.Funciones;


/**
 *
 * @author BlackBana
 */
public class ModeloMysql implements IModelo {
    private final String bbdd = "1516ceed82prg";
    private final String usuario = "alumno";
    private final String password = "alumno";
    private final String jdbcUrl = "jdbc:mysql://localhost/" + bbdd;
    private final String driver = "com.mysql.jdbc.Driver";
    private static final SimpleDateFormat recuperarfecha = new SimpleDateFormat("yyyy-MM-dd");
    private MysqlConnectionPoolDataSource mcpds = new MysqlConnectionPoolDataSource();
    private PooledConnection pc = null;
    private Connection cn = null;
    private Statement st = null;
    private ResultSet rs = null;
    private String error, sql;
    private Funciones f = new Funciones();
    
    
  public ModeloMysql() {
      mcpds.setUser(usuario);
      mcpds.setPassword(password);
      mcpds.setUrl(jdbcUrl);
    }
  
    
    public String crearTablas() {
        error = null;
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            
            sql = "DROP TABLE IF EXISTS apadrina;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "DROP TABLE IF EXISTS padrinos;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS perros;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "CREATE TABLE IF NOT EXISTS `padrinos` (\n"
               + "  `idpersona` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `nombrepersona` varchar(20),\n"
               + "  `telefono` varchar(15),\n"
               + "  `email` varchar(50),\n"
               + "  PRIMARY KEY (`idpersona`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);


            sql = "CREATE TABLE IF NOT EXISTS `perros` (\n"
               + "  `idperro` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `nombreperro` varchar(20),\n"
               + "  `nchip` BIGINT(15),\n"
               + "  `raza` varchar(15),\n"
               + "  PRIMARY KEY (`idperro`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);

            
            sql = "CREATE TABLE IF NOT EXISTS `apadrina` (\n"
               + "  `idapadrina` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `idpadrino_apa` int(5) NOT NULL,\n"
               + "  `idperro_apa` int(5) NOT NULL,\n"
               + "  `fechaapa` date NOT NULL,\n"
               + "  PRIMARY KEY (`idapadrina`),\n"    
               + "  FOREIGN KEY (`idpadrino_apa`) REFERENCES padrinos(idpersona),\n"
               + "  FOREIGN KEY (`idperro_apa`) REFERENCES perros(idperro)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0\n ;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            Funciones.mensaje(null, "Tablas creadas con éxito.", "CREACION DE TABLAS",1);
            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            Funciones.mensaje(null, "Fallo en la creación de las tablas. \nError: " + ex.getMessage(), "CREACION DE TABLAS",0);
        }
        desconectarBd();
        return error;
    }
    
    public String insertarDatos() {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino1','1111111', 'padrino1@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
			
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino2','2222222', 'padrino2@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO padrinos(nombrepersona,telefono,email) VALUES('Padrino3','33333333', 'padrino3@padrinos.es');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro1', 123456789012345,'mestizo');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro2', 123456789012345,'Raza1');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO perros(nombreperro,nchip,raza) VALUES('Perro3', 123456789012345,'Raza2');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO apadrina (idpadrino_apa,idperro_apa,fechaapa) VALUES(1, 1,'2016/05/12');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO apadrina (idpadrino_apa,idperro_apa,fechaapa) VALUES(2, 2,'2016/01/17');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            Funciones.mensaje(null, "Datos genéricos insertados con éxito.", "INSERCION DE DATOS",1);
            
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            Funciones.mensaje(null, "Fallo en la inserción de datos genéricos. \nError: " + ex.getMessage(), "INSERCION DE DATOS",0);
        }
        desconectarBd();
        return error;
            
    }
	
	public void conectarBd() {
        try {
            pc = mcpds.getPooledConnection();
            cn = pc.getConnection();
            System.out.println("Base de datos " + bbdd + ": conectada");
        } catch (SQLException se) {
            se.printStackTrace();
            Funciones.mensaje(null, "Fallo al conectar con la base de datos.\nError: " + se.getMessage(), "CONEXION CON BASE DE DATOS",0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void desconectarBd() {
        try {
            System.out.println("Base de datos " + bbdd + ": desconectada");
            cn.close();
        } catch (SQLException se) {
            se.printStackTrace();
            Funciones.mensaje(null, "Fallo al desconectar de la base de datos. \nError: " + se.getMessage(), "CONEXION CON BASE DE DATOS",0);
        }
    }

    
    @Override
    public void create(Padrinos padrino) {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "INSERT INTO padrinos (nombrepersona, telefono, email) VALUES ('" + padrino.getNombrepersona() + "', '" + padrino.getTelefono() + "', '" + padrino.getEmail() + "');";
            System.out.println(sql);
            st.executeUpdate(sql);
            Funciones.mensaje(null, "Padrino creado con éxito.", "PADRINOS EN MYSQL",1);
             
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al crear el padrino \nError: " + se.getMessage(), "CREAR PADRINOS EN MYSQL",0);
        }
        
    }

    @Override
    public ArrayList<Padrinos> readPadrino() {
        ArrayList<Padrinos> padrinos = new ArrayList();
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "SELECT idpersona, nombrepersona, telefono, email\n FROM padrinos\n ORDER BY idpersona;";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while (rs.next()) {
               Padrinos padrino = new Padrinos();
               
               padrino.setIdpersona(rs.getInt("idpersona"));
               padrino.setNombrepersona(rs.getString("nombrepersona"));
               padrino.setTelefono(rs.getString("telefono"));
               padrino.setEmail(rs.getString("email"));
               
               padrinos.add(padrino);
            }
        } catch (SQLException se) {
          se.printStackTrace();
          Funciones.mensaje(null, "Fallo al leer en la base de datos. \nError: " + se.getMessage(), "LEER PADRINOS EN MYSQL",0);
        }
        catch (Exception e) {
          e.printStackTrace();
          Funciones.mensaje(null, "Fallo al leer en la base de datos. \nError: " + e.getMessage(), "LEER PADRINOS EN MYSQL",0);
        }
        desconectarBd();
        return padrinos;
    }

    @Override
    public void update(Padrinos padrino) {
           try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "UPDATE padrinos SET nombrepersona = '" + padrino.getNombrepersona() + "', telefono = '" 
                    + padrino.getTelefono() + "', email = '" + padrino.getEmail() + "' WHERE idpersona = '" + padrino.getIdpersona() + "';";
            System.out.println(sql);
            st.executeUpdate(sql);
            desconectarBd();
            Funciones.mensaje(null, "Padrino actualizado con éxito", "ACTUALIZAR PADRINOS EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al actualizar el padrino la base de datos. \nError: " + se.getMessage(), "ACTUALIZAR PADRINOS EN MYSQL",0);
        }
    }

    @Override
    public void delete(Padrinos padrino) {
         try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "DELETE FROM padrinos WHERE idpersona = '" + padrino.getIdpersona() + "';";
            System.out.println(sql);
            st.executeUpdate(sql);
            desconectarBd();
            Funciones.mensaje(null, "Padrino borrado con éxito", "BORRAR PADRINOS EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al eliminar el padrino en la base de datos. \nError: " + se.getMessage(), "BORRAR PADRINOS EN MYSQL",0);
        }
    }

    @Override
    public void create(Perros perro) {
            try {
                conectarBd();
                st = (Statement) cn.createStatement();
                sql = "INSERT INTO perros (nombreperro, nchip, raza) VALUES ('" + perro.getNombreperro() + "', '" + perro.getNchip() + "', '" + perro.getRaza() + "');";
                System.out.println(sql);
                st.executeUpdate(sql);
                Funciones.mensaje(null, "Perro creado con éxito", "CREAR PERRO EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al crear el perro. \nError: " + se.getMessage() , "CREAR PERRO EN MYSQL",0);

        }
    }

    @Override
    public ArrayList<Perros> readPerro() {
        ArrayList<Perros> perros = new ArrayList();
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "SELECT idperro, nombreperro, nchip, raza FROM perros ORDER BY idperro;";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Perros perro = new Perros();
                perro.setIdperro(rs.getInt("idperro"));
                perro.setNombreperro(rs.getString("nombreperro"));
                perro.setNchip(rs.getLong("nchip"));
                perro.setRaza(rs.getString("raza"));
                
                perros.add(perro);
            }
        } catch (SQLException se) {
          se.printStackTrace();
          Funciones.mensaje(null, "Fallo al leer en la base de datos. \nError: " + se.getMessage() , "LEER PERRO EN MYSQL",0);
        }
        catch (Exception e) {
          e.printStackTrace();
          Funciones.mensaje(null, "Fallo al leer en la base de datos. \nError: " + e.getMessage() , "LEER PERRO EN MYSQL",0);
        }
        desconectarBd();
        return perros;
    }

    @Override
    public void update(Perros perro) {
           try {
                conectarBd();
                st = (Statement) cn.createStatement();
                sql = "UPDATE perros SET nombreperro = '" + perro.getNombreperro() + "', nchip = '" 
                        + perro.getNchip() + "', raza = '" + perro.getRaza() + "' WHERE idperro = '" + perro.getIdperro() + "';";
                System.out.println(sql);
                st.executeUpdate(sql);
                desconectarBd();
                Funciones.mensaje(null, "Perro actualizado con éxito.", "ACTUALIZAR PERRO EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al actualizar el perro. \nError: " + se.getMessage() , "LEER PERRO EN MYSQL",0);
        }
    }

    @Override
    public void delete(Perros perro) {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "DELETE FROM perros WHERE idperro = '" + perro.getIdperro() + "';";
            System.out.println(sql);
            st.executeUpdate(sql);
            desconectarBd();
            Funciones.mensaje(null, "Perro borrado con éxito.", "BORRAR PERRO EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al borrar el perro. \nError: " + se.getMessage() , "BORRAR PERRO EN MYSQL",0);
        }    }

    @Override
    public void create(Apadrina apadrina) {
          try {
                conectarBd();   
                st = (Statement) cn.createStatement();
                sql = "INSERT INTO apadrina (idpadrino_apa, idperro_apa, fechaapa) VALUES ('" + apadrina.getPadrinos().getIdpersona() + "', '" + apadrina.getPerros().getIdperro() + "', '" + recuperarfecha.format(apadrina.getFechaapa()) + "');";
                System.out.println(sql);
                st.executeUpdate(sql);
                Funciones.mensaje(null, "Apadrinamiento creado con éxito.", "CREAR APADRINAMIENTO EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al crear el apadrinamiento. \nError: " + se.getMessage() , "\"CREAR APADRINAMIENTO EN MYSQL",0);
        }    }

    @Override
      public ArrayList<Apadrina> readApadrina() {
        ArrayList<Apadrina> apadrinas = new ArrayList();
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
//            sql = "SELECT idapadrina, idpadrino_apa, idperro_apa, fechaapa FROM apadrina ORDER BY idapadrina;";
            sql = "SELECT apa.idapadrina, apa.fechaapa, pa.idpersona, pa.nombrepersona, pa.telefono, pa.email, "
                    + "pe.idperro, pe.nombreperro, pe.nchip, pe.raza "
                    + "FROM apadrina apa, padrinos pa, perros pe "
                    + "WHERE apa.idperro_apa = pa.idPersona AND apa.idpadrino_apa = pe.idperro;";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Apadrina apadrina = new Apadrina();
                Padrinos padrino = new Padrinos();
                Perros perro = new Perros();
                
                apadrina.setIdapadrina(rs.getInt("apa.idapadrina"));
                apadrina.setFechaapa(recuperarfecha.parse(rs.getString("fechaapa")));
                
                padrino.setIdpersona(rs.getInt("pa.idpersona"));
                padrino.setNombrepersona(rs.getString("pa.nombrepersona"));
                padrino.setTelefono(rs.getString("pa.telefono"));
                padrino.setEmail(rs.getString("pa.email"));
                
                perro.setIdperro(rs.getInt("pe.idperro"));
                perro.setNombreperro(rs.getString("pe.nombreperro"));
                perro.setNchip(rs.getLong("pe.nchip"));
                perro.setRaza(rs.getString("pe.raza"));

                apadrina.setPadrinos(padrino);
                apadrina.setPerros(perro);
                apadrinas.add(apadrina);
            }
        } catch (SQLException se) {
          se.printStackTrace();
           Funciones.mensaje(null, "Fallo al leer en la base de datos. \nError: " + se.getMessage() , "\"LEER APADRINAMIENTO EN MYSQL",0);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        desconectarBd();
        return apadrinas;    }
     
    @Override
    public void update(Apadrina apadrina) {
        try {
                conectarBd();
                st = (Statement) cn.createStatement();
                sql = "UPDATE apadrina SET idpadrino_apa = '" 
                        + apadrina.getPadrinos().getIdpersona() + "', idperro_apa = '" + apadrina.getPerros().getIdperro() + "', fechaapa = '" + recuperarfecha.format(apadrina.getFechaapa()) + "' WHERE idapadrina = '" + apadrina.getIdapadrina() + "';";
                System.out.println(sql);
                st.executeUpdate(sql);
                desconectarBd();
                Funciones.mensaje(null, "Apadrinamiento actualizado con éxito.", "ACTUALIZAR APADRINAMIENTO EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al actualizar el apadrinamiento. \nError: " + se.getMessage() , "\"ACTUALIZACIÓN APADRINAMIENTO EN MYSQL",0);
        }  
    }

    @Override
    public void delete(Apadrina apadrina) {
        try {
            conectarBd();
            st = (Statement) cn.createStatement();
            sql = "DELETE FROM apadrina WHERE idapadrina = '" + apadrina.getIdapadrina() + "';";
            System.out.println(sql);
            st.executeUpdate(sql);
            desconectarBd();
            Funciones.mensaje(null, "Apadrinamiento borrado con éxito.", "BORRADO APADRINAMIENTO EN MYSQL",1);
        } catch (SQLException se) {
             se.printStackTrace();
             Funciones.mensaje(null, "Fallo al borrar el apadrinamiento. \nError: " + se.getMessage() , "\"BORRAR APADRINAMIENTO EN MYSQL",0);
        }
    }

    @Override
    public void instalarDb() {
        st = null;
        error = null;  
        try {
            Class.forName(driver).newInstance();
            cn = ((Connection)DriverManager.getConnection("jdbc:mysql://localhost/", usuario, password));
            st = cn.createStatement();  
            sql = "CREATE DATABASE IF NOT EXISTS " + bbdd + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
//            f.info("Base de datos " + bbdd + " creada correctamente");
            crearTablas();
            insertarDatos();
        } catch (Exception e) {
            Funciones.mensaje(null, "Fallo en la instalación de la base de datos. \nError: " + e.getMessage(), "CREACION DE LA BASE DE DATOS",0);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    Funciones.mensaje(null, "Fallo en la instalación de la base de datos. \nError: " + e.getMessage(), "CREACION DE LA BASE DE DATOS",0);

                }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException se) {
                    Funciones.mensaje(null, "Fallo en la instalación de la base de datos. \nError: " + se.getMessage(), "CREACION DE LA BASE DE DATOS",0);
                }
            }
        }
    }

    @Override
    public void desinstalarDb() {
        cn = null;
  	st = null;
  	error = null;
  	try {
            conectarBd();
            st = cn.createStatement();
            sql = "DROP DATABASE IF EXISTS " + bbdd + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
  	} catch (Exception e) {
            Funciones.mensaje(null, "Fallo en la desinstalación de la base de datos. \nError: " + e.getMessage(), "ELIMINACIÓN DE LA BASE DE DATOS",0);
  	} finally {
            if (st != null) {
                    try {
                    st.close();
                    } catch (SQLException e) {
                        Funciones.mensaje(null, "Fallo en la desinstalación de la base de datos. \nError: " + e.getMessage(), "ELIMINACIÓN DE LA BASE DE DATOS",0);
                    }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException se) {
                    Funciones.mensaje(null, "Fallo en la desinstalación de la base de datos. \nError: " + se.getMessage(), "ELIMINACIÓN DE LA BASE DE DATOS",0);
                }
            }
        }
    }

    
}
