package org.ceed.ceed82prgpro10.modelo;

import java.util.ArrayList;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/


public interface IModelo {

      	//Padrino
	public void create(Padrinos padrino);
	public ArrayList <Padrinos> readPadrino();
	public void update(Padrinos padrino);
	public void delete(Padrinos padrino);
	

	//Perro
	public void create(Perros perro);
	public ArrayList <Perros> readPerro();
	public void update(Perros perro);
	public void delete(Perros perro);
	

	//Apadrina
	public void create(Apadrina apadrina);
	public ArrayList <Apadrina> readApadrina();
	public void update(Apadrina apadrina);
	public void delete(Apadrina apadrina);
        
        //Genericos
        
        public void instalarDb();
        public void desinstalarDb();
        public void conectarBd();
	public void desconectarBd();
}
