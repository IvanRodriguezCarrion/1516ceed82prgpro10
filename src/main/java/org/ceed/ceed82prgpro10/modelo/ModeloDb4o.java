package org.ceed.ceed82prgpro10.modelo;


import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.ceed.ceed82prgpro10.vista.Funciones;


/**
 *
 * @author BlackBana
 */
public class ModeloDb4o implements IModelo {
  private int idpadrino = 0;
  private int idperro = 0;
  private int idapadrina = 0;
  private static final SimpleDateFormat recuperarfecha = new SimpleDateFormat("yyyy-MM-dd");
  private String error;
  private static String nomArchivo = "bdoo.db4o";
  private File archDb4O = new File(nomArchivo);
  private ObjectContainer bd;
  
  public ModeloDb4o() {
      if (!this.archDb4O.exists())
          {
            Funciones.mensaje(null, "No existe BD. Debes instalarla primero.", "Error en el archivo DB4O", 1);
          }
    }
    
  @Override
    public void instalarDb() {
        try {
        Padrinos padrino1 = new Padrinos ("Padrino1", "96-3225445","padrino1@padrino1.es", null);
        Padrinos padrino2 = new Padrinos ("Padrino2", "65805777","padrino2@padrino2.es", null);
        Padrinos padrino3 = new Padrinos ("Padrino3", "668203387","padrino3@padrino3.es", null);
        
	long nchip = Long.parseLong("123456789012345");
	
        Perros perro1 = new Perros("Perro1", nchip, "Raza 1", null);
        Perros perro2 = new Perros("Perro2", nchip, "Raza 2", null);
        Perros perro3 = new Perros("Perro3", nchip, "Raza 3", null);

        Apadrina apadrina1 = new Apadrina(padrino1, perro1, recuperarfecha.parse("2016-12-05"));
        Apadrina apadrina2 = new Apadrina(padrino2, perro2, recuperarfecha.parse("2016-05-07"));
        Apadrina apadrina3 = new Apadrina(padrino3, perro3, recuperarfecha.parse("2016-01-01"));
        create(padrino1);
        create(padrino2);
        create(padrino3);

        create(perro1);
        create(perro2);
        create(perro3);

        create(apadrina1);
        create(apadrina2);
        create(apadrina3);
        
      } catch (ParseException ex) {
          Funciones.mensaje(null, "Error en la instalación de la base de datos", "Error en la BDOO", 1);
      }
                 
    }
    
  @Override
    public void desinstalarDb() {
        archDb4O.delete();
        System.out.println("Archivo archDb4O borrado");
    }

    
    
    public void conectarBd() {
        System.out.println("DB4O conectada");
        this.bd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), nomArchivo);
    }
    
    public void desconectarBd() {   
        System.out.println("Connexión cerrada");
        this.bd.close();
    }
    
    @Override
    public void create(Padrinos padrino) {
        idpadrino = calcularIdPad();
        conectarBd();
        padrino.setIdpersona(idpadrino);
        idpadrino += 1;
        bd.store(padrino);
        desconectarBd();  
    }

    @Override
    public ArrayList<Padrinos> readPadrino() {
        ArrayList<Padrinos> padrinos = new ArrayList();
        Padrinos padrino_tipo = new Padrinos (null, null, null, null); 
        Padrinos pad_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              pad_bdo = (Padrinos)res.next();
              padrinos.add(pad_bdo);
            }
            desconectarBd();
        return padrinos;
    }

    @Override
    public void update(Padrinos padrino) {
        Padrinos padrino_tipo = new Padrinos (padrino.getIdpersona(), null, null, null); 
        Padrinos pad_actualizar = null;
        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // El motor de búsqueda, en este caso, buscará unicamente los objetos que tengan el parámetro que le hemos definido (pasando del resto pues son null)
        if (res.hasNext())
            {
              pad_actualizar = (Padrinos)res.next();
              pad_actualizar.setNombrepersona(padrino.getNombrepersona());
              pad_actualizar.setEmail(padrino.getEmail());
              pad_actualizar.setTelefono(padrino.getTelefono());
              bd.store(pad_actualizar);
            }
            desconectarBd();
  
        
    }

    @Override
    public void delete(Padrinos padrino) {
        Padrinos padrino_tipo = new Padrinos (padrino.getIdpersona(), null, null, null); 
        Padrinos pad_borrar = null;
        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // El motor de búsqueda, en este caso, buscará unicamente los objetos que tengan el parámetro que le hemos definido (pasando del resto pues son null)
        if (res.hasNext())
            {
              pad_borrar = (Padrinos)res.next();
              bd.delete(pad_borrar);
            }
            desconectarBd();
  
    }

    @Override
    public void create(Perros perro) {
        idperro = calcularIdPer();
        conectarBd();
        perro.setIdperro(idperro);
        idperro += 1;
        bd.store(perro);
        desconectarBd();       
    }

    @Override
    public ArrayList<Perros> readPerro() {
        ArrayList<Perros> perros = new ArrayList();
        Perros perro_tipo = new Perros (0, null, null, null); 
        Perros per_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_bdo = (Perros)res.next();
              perros.add(per_bdo);
            }
            desconectarBd();
        return perros;
    }

    @Override
    public void update(Perros perro) {
        Perros perro_tipo = new Perros (perro.getIdperro(), null, null, null); 
        Perros per_actualizar = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_actualizar = (Perros)res.next();
              per_actualizar.setNombreperro(perro.getNombreperro());
              per_actualizar.setNchip(perro.getNchip());
              per_actualizar.setRaza(perro.getRaza());
            }
            desconectarBd();     
    }

    @Override
    public void delete(Perros perro) {
        Perros perro_tipo = new Perros (perro.getIdperro(), null, null, null); 
        Perros per_borrar = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_borrar = (Perros)res.next();
              bd.delete(per_borrar);
            }
            desconectarBd();     
    }

    @Override
    public void create(Apadrina apadrina) {
        idapadrina = calcularIdApa();
        conectarBd();
        apadrina.setIdapadrina(idapadrina);
        Padrinos pa = recuperarPadrino(apadrina);
        Perros pe = recuperarPerro(apadrina);
        if (pa != null) {
          apadrina.setPadrinos(pa);
        }
        if (pe != null) {
          apadrina.setPerros(pe); 
        }

        idapadrina += 1;
        bd.store(apadrina);
        desconectarBd();
    }

    @Override
    public ArrayList<Apadrina> readApadrina() {
        ArrayList<Apadrina> apadrinas = new ArrayList();
        Apadrina apadrina_tipo = new Apadrina (0, null, null, null); 
        Apadrina apa_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(apadrina_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_bdo = (Apadrina)res.next();
              apadrinas.add(apa_bdo);
            }
            desconectarBd();
        return apadrinas;    }

    @Override
    public void update(Apadrina apadrina) {
     Apadrina apadrina_tipo = new Apadrina (apadrina.getIdapadrina(), null, null, null); 
     Apadrina apa_actualizar = null;

     conectarBd();

     ObjectSet res = this.bd.queryByExample(apadrina_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_actualizar = (Apadrina)res.next();
              Padrinos pa = recuperarPadrino(apadrina);
              Perros pe = recuperarPerro(apadrina);
              if (pa != null) {
                  apa_actualizar.setPadrinos(pa);
              }
              if (pe != null) {
                  apa_actualizar.setPerros(pe); 
              }
              apa_actualizar.setFechaapa(apadrina.getFechaapa());
            }
            bd.store(apa_actualizar);
            desconectarBd();
    }

    @Override
    public void delete(Apadrina apadrina) {
        Apadrina apadrina_tipo = new Apadrina (apadrina.getIdapadrina(), null, null, null); 
        Apadrina apa_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(apadrina_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_bdo = (Apadrina)res.next();
              bd.delete(apa_bdo);
            }
            desconectarBd();
    }
    
    
    private int calcularIdPad() {
        int idPadSig = 0;
        // padrino_tipo lo que le pasaremos al comparador para que busque en el archivo todos los objetos del mismo tipo.
        Padrinos padrino_tipo = new Padrinos (0, null, null, null); 
        Padrinos pad_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(padrino_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              pad_bdo = (Padrinos)res.next();
              int id = pad_bdo.getIdpersona();
              if (id > idPadSig) {
                idPadSig = id;
              }
              //Este IF está justificado pensando en que desconocemos la ordenación que va a hacer el motor de búsqueda a la hora de ordenar los objetos. 
              //Si los ordena de forma aleatoria y no hiciesemos un if controlando la ID, cogería la última que almacenase y podría ser que no fuese la más alta.
            }
            desconectarBd();
        return idPadSig + 1;
    }
    
    private int calcularIdPer() {
        int idPerSig = 0;
        // perro_tipo lo que le pasaremos al comparador para que busque en el archivo todos los objetos del mismo tipo.
        Perros perro_tipo = new Perros (0, null, null, null); 
        Perros per_bdo = null;

        conectarBd();

        ObjectSet res = bd.queryByExample(perro_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              per_bdo = (Perros)res.next();
              int id = per_bdo.getIdperro();
              if (id > idPerSig) {
                idPerSig = id;
              }
              //Este IF está justificado pensando en que desconocemos la ordenación que va a hacer el motor de búsqueda a la hora de ordenar los objetos. 
              //Si los ordena de forma aleatoria y no hiciesemos un if controlando la ID, cogería la última que almacenase y podría ser que no fuese la más alta.
            }
            desconectarBd();
        return idPerSig + 1;
    }
    
    private int calcularIdApa() {
        int idApaSig = 0;
        // perro_tipo lo que le pasaremos al comparador para que busque en el archivo todos los objetos del mismo tipo.

        Apadrina apa_tipo = new Apadrina (0, null, null, null); 
        Apadrina apa_bdo = null;

        conectarBd();

        ObjectSet res = this.bd.queryByExample(apa_tipo); // Motor de búsqueda de objetos del tipo que le pasemos, recogiéndolos dentro de la variable res.
        while (res.hasNext())
            {
              apa_bdo = (Apadrina)res.next();
              int id = apa_bdo.getIdapadrina();
              if (id > idApaSig) {
                idApaSig = id;
              }
              //Este IF está justificado pensando en que desconocemos la ordenación que va a hacer el motor de búsqueda a la hora de ordenar los objetos. 
              //Si los ordena de forma aleatoria y no hiciesemos un if controlando la ID, cogería la última que almacenase y podría ser que no fuese la más alta.
            }
            desconectarBd();
        return idApaSig + 1;
    }
    // Con estos métodos nos aseguramos de que el objeto que recuperamos es el MISMO que tenemos en la BDOO y no uno nuevo, lo que nos produciría duplicidades en el registro.
    
    private Padrinos recuperarPadrino(Apadrina apadrina) {
        Padrinos padrino_tipo = new Padrinos (apadrina.getPadrinos().getIdpersona(), null, null, null); 
        Padrinos pad_recuperar = null;
        ObjectSet res = bd.queryByExample(padrino_tipo);
        pad_recuperar = (Padrinos)res.next();
        return pad_recuperar;
    }
    
    private Perros recuperarPerro(Apadrina apadrina) {
        Perros perro_tipo = new Perros (apadrina.getPerros().getIdperro(), null, null, null); 
        Perros per_recuperar = null;
        ObjectSet res = bd.queryByExample(perro_tipo);
        per_recuperar = (Perros)res.next();
        return per_recuperar;
    }
    

    
}
