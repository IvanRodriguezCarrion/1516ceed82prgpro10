package org.ceed.ceed82prgpro10.vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.RollbackException;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class VistaGraficaPerTab extends JPanel implements ActionListener {
    private javax.swing.JButton botonBorrar;
    private javax.swing.JButton botonCrear;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton botonRecargar;
    private javax.persistence.EntityManager entityManager;
    private javax.swing.JTextField idperroField;
    private javax.swing.JLabel idperroLabel;
    private java.util.List<org.ceed.ceed82prgpro10.vista.Perros> list;
    private javax.swing.JScrollPane masterScrollPane;
    private javax.swing.JTable masterTable;
    private javax.swing.JTextField nchipField;
    private javax.swing.JLabel nchipLabel;
    private javax.swing.JTextField nombreperroField;
    private javax.swing.JLabel nombreperroLabel;
    private javax.persistence.Query query;
    private javax.swing.JTextField razaField;
    private javax.swing.JLabel razaLabel;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    
    public VistaGraficaPerTab() {
	bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        entityManager = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory("1516ceed82prg?zeroDateTimeBehavior=convertToNullPU").createEntityManager();
        query = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery("SELECT p FROM Perros p");
        list = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(query.getResultList());
        masterScrollPane = new javax.swing.JScrollPane();
        masterTable = new javax.swing.JTable();
        idperroLabel = new javax.swing.JLabel();
        nombreperroLabel = new javax.swing.JLabel();
        nchipLabel = new javax.swing.JLabel();
        razaLabel = new javax.swing.JLabel();
        idperroField = new javax.swing.JTextField();
        nombreperroField = new javax.swing.JTextField();
        nchipField = new javax.swing.JTextField();
        razaField = new javax.swing.JTextField();
        botonGuardar = new javax.swing.JButton();
        botonRecargar = new javax.swing.JButton();
        botonCrear = new javax.swing.JButton();
        botonBorrar = new javax.swing.JButton();
	
	org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, list, masterTable);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${idperro}"));
        columnBinding.setColumnName("Identificador");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nombreperro}"));
        columnBinding.setColumnName("Nombre");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nchip}"));
        columnBinding.setColumnName("Número de Chip");
        columnBinding.setColumnClass(java.math.BigInteger.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${raza}"));
        columnBinding.setColumnName("Raza");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        masterScrollPane.setViewportView(masterTable);

        idperroLabel.setText("Identificador:");

        nombreperroLabel.setText("Nombre:");

        nchipLabel.setText("Número Chop:");

        razaLabel.setText("Raza:");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.idperro}"), idperroField, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Inserte aquí el identificador");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), idperroField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.nombreperro}"), nombreperroField, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Inserte aquí el nombre del perro");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), nombreperroField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.nchip}"), nchipField, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Inserte aquí el número de chip con el formato de 15 dígitos cualesquiera");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), nchipField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.raza}"), razaField, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Inserte aquí la raza");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), razaField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        botonGuardar.setText("Guardar");
        botonGuardar.addActionListener(this);

        botonRecargar.setText("Recargar");
        botonRecargar.addActionListener(this);

        botonCrear.setText("Crear");
        botonCrear.addActionListener(this);

        botonBorrar.setText("Borrar");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), botonBorrar, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        botonBorrar.addActionListener(this);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(botonCrear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonBorrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonRecargar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonGuardar))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(idperroLabel)
                                    .addComponent(nombreperroLabel)
                                    .addComponent(nchipLabel)
                                    .addComponent(razaLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(idperroField, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                                    .addComponent(nombreperroField, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                                    .addComponent(nchipField, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                                    .addComponent(razaField, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)))
                            .addComponent(masterScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {botonBorrar, botonCrear, botonGuardar, botonRecargar});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(masterScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idperroLabel)
                    .addComponent(idperroField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombreperroLabel)
                    .addComponent(nombreperroField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nchipLabel)
                    .addComponent(nchipField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(razaLabel)
                    .addComponent(razaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonGuardar)
                    .addComponent(botonRecargar)
                    .addComponent(botonBorrar)
                    .addComponent(botonCrear))
                .addContainerGap())
        );

        bindingGroup.bind();
	if (!Beans.isDesignTime()) {
	    entityManager.getTransaction().begin();
	}
    }
    
    public void actionPerformed(java.awt.event.ActionEvent evt) {
            if (evt.getSource() == botonGuardar) {
                botonGuardarActionPerformed(evt);
            }
            else if (evt.getSource() == botonRecargar) {
                botonRecargarActionPerformed(evt);
            }
            else if (evt.getSource() == botonCrear) {
                botonCrearActionPerformed(evt);
            }
            else if (evt.getSource() == botonBorrar) {
                botonBorrarActionPerformed(evt);
            }
        }
    
    private void botonRecargarActionPerformed(java.awt.event.ActionEvent evt) {                                              
	entityManager.getTransaction().rollback();
	entityManager.getTransaction().begin();
	java.util.Collection data = query.getResultList();
	for (Object entity : data) {
	    entityManager.refresh(entity);
	}
	list.clear();
	list.addAll(data);
    }                                             

    private void botonBorrarActionPerformed(java.awt.event.ActionEvent evt) {                                            
	int[] selected = masterTable.getSelectedRows();
	List<org.ceed.ceed82prgpro10.vista.Perros> toRemove = new ArrayList<org.ceed.ceed82prgpro10.vista.Perros>(selected.length);
	for (int idx = 0; idx < selected.length; idx++) {
	    org.ceed.ceed82prgpro10.vista.Perros p = list.get(masterTable.convertRowIndexToModel(selected[idx]));
	    toRemove.add(p);
	    entityManager.remove(p);
	}
	list.removeAll(toRemove);
    }                                           

    private void botonCrearActionPerformed(java.awt.event.ActionEvent evt) {                                           
	org.ceed.ceed82prgpro10.vista.Perros p = new org.ceed.ceed82prgpro10.vista.Perros();
	entityManager.persist(p);
	list.add(p);
	int row = list.size() - 1;
	masterTable.setRowSelectionInterval(row, row);
	masterTable.scrollRectToVisible(masterTable.getCellRect(row, 0, true));
    }                                          
    
    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {                                             
	try {
	    entityManager.getTransaction().commit();
	    entityManager.getTransaction().begin();
	} catch (RollbackException rex) {
	    rex.printStackTrace();
	    entityManager.getTransaction().begin();
	    List<org.ceed.ceed82prgpro10.vista.Perros> merged = new ArrayList<org.ceed.ceed82prgpro10.vista.Perros>(list.size());
	    for (org.ceed.ceed82prgpro10.vista.Perros p : list) {
		merged.add(entityManager.merge(p));
	    }
	    list.clear();
	    list.addAll(merged);
	}
    }
    
}