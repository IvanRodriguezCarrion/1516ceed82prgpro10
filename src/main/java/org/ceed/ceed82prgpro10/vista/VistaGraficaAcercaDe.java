package org.ceed.ceed82prgpro10.vista;
/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/
import java.awt.FlowLayout; // Gestor de contenido tipo Flow
import java.awt.BorderLayout; // Gestor de contenido tipo Border.
import javax.swing.JInternalFrame; // Poder crear J.Frames
import javax.swing.JPanel; // Para crear paneles
import javax.swing.JLabel; // Para crear textos
import javax.swing.JButton; // Para crear botones
import javax.swing.ImageIcon; // Permite insertar iconos.


public class VistaGraficaAcercaDe extends JInternalFrame{
    private JInternalFrame contenedorPrincipal = new JInternalFrame(); // Contedra todos los componentes de la vista principal
    private JPanel panel1 = new JPanel(); // Aquí ira el avatar-
    public static VistaGraficaAcercaDe instancia;
    
    private JLabel labelNombre = new JLabel("Ivan Rodriguez Carrion (Ringo S.L)    Código del curso: 1516CEED82", JLabel.CENTER);
    private JLabel lblRingo = new JLabel("  ");
    private ImageIcon imageRingo = new ImageIcon(getClass().getResource("/imagenes/ringo.png"));

    private JButton botonSalir = new JButton ("Salir");
    
    private VistaGraficaAcercaDe() {
        menuAcercaDe();
    }
    
    private void menuAcercaDe() {
        setTitle("Acerca de...");
        setSize(450,200);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //Indicamos que la X en esta ventana cerrará el programa. En el resto de ventanas estará deshabilitado
        setResizable(false); // Con esto evitamos que se redimensione la ventana
        BorderLayout bordes = new BorderLayout(); // Gestor de contenido de toda esta ventan
        setLayout(bordes); // lo seteamos
        lblRingo.setIcon(imageRingo); // Con esto insertamos en el label de Ringo su imagen, para que actue de fondo del mismo.
        panel1.setLayout(new FlowLayout(FlowLayout.CENTER)); // Creamos el Flowlayout que permite insertarlo en el centro
        panel1.add(lblRingo); //Lo añadimos
        add(labelNombre, BorderLayout.NORTH); //Colocación de los elementos.
        add(panel1, BorderLayout.CENTER);
        add(botonSalir, BorderLayout.SOUTH);

//        setVisible(true);
    }
    
    public JButton getBotonSalir(){
        return botonSalir;
        }  
    
    public static VistaGraficaAcercaDe getInstancia() {
        if (instancia == null) {
           instancia = new VistaGraficaAcercaDe(); 
        } 
        return instancia;
    }
}
