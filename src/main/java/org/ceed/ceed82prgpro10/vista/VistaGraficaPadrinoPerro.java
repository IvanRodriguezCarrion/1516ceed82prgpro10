/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceed.ceed82prgpro10.vista;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;

/**
 *
 * @author BlackBana
 */
public class VistaGraficaPadrinoPerro extends JInternalFrame {
    private static VistaGraficaPadrinoPerro instancia;
    private JPanel jPanelDominante = new JPanel();
    private JScrollPane jScrollPane1 = new JScrollPane();
    private JTable jTDominante = new JTable();
    private JButton botonIrPad = new JButton();
    private JPanel jPanelRecesivo = new JPanel();
    private JScrollPane jScrollPane2 = new JScrollPane();
    private JTable jTRecesivo = new JTable();
    private JButton botonIrPer = new JButton();
    private JSlider jSliderPadPer = new JSlider();
    private JLabel jLblPer = new JLabel();
    private JLabel jlblpad = new JLabel();
    private JButton botonRecargar = new JButton();
    private JButton botonSalir = new JButton();
    private JButton botonIrApa = new JButton();
    
    private VistaGraficaPadrinoPerro() {
        
        super("Relación Padrinos - Perros");
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setSize(700,400);
        setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE); //Indicamos que la X en esta ventana no haga nada, ya que queremos que se cierre desde cancelar.
        setResizable(false); // Con esto evitamos que se redimensione la ventana
        
       jPanelDominante.setBorder(javax.swing.BorderFactory.createTitledBorder("Padrinos"));
        
        jScrollPane1.setViewportView(jTDominante);

        javax.swing.GroupLayout jPanelPadLayout = new javax.swing.GroupLayout(getPanelDominante());
        jPanelDominante.setLayout(jPanelPadLayout);
        jPanelPadLayout.setHorizontalGroup(
            jPanelPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPadLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 466, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelPadLayout.setVerticalGroup(
            jPanelPadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanelRecesivo.setBorder(javax.swing.BorderFactory.createTitledBorder("Perros"));
        jPanelRecesivo.setPreferredSize(new java.awt.Dimension(647, 163));
        
        jScrollPane2.setViewportView(jTRecesivo);

        javax.swing.GroupLayout jPanelPeLayout = new javax.swing.GroupLayout(getPanelRecesivo());
        jPanelRecesivo.setLayout(jPanelPeLayout);
        jPanelPeLayout.setHorizontalGroup(
            jPanelPeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPeLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(122, Short.MAX_VALUE))
        );
        jPanelPeLayout.setVerticalGroup(
            jPanelPeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSliderPadPer.setMaximum(1);
        jSliderPadPer.setOrientation(javax.swing.JSlider.VERTICAL);
        jSliderPadPer.setValue(0);

        jLblPer.setText("Relaciona Perros");

        jlblpad.setText("Relaciona Padrinos");

        botonRecargar.setText("Recargar");
        
        botonSalir.setText("Salir");

        botonIrApa.setText("Ir a Apadrinamientos");

        botonIrPad.setText("Ir a Padrinos");

        botonIrPer.setText("Ir a Perros");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelDominante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelRecesivo, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLblPer)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(43, 43, 43)
                            .addComponent(jSliderPadPer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(37, 37, 37)))
                    .addComponent(jlblpad))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(99, Short.MAX_VALUE)
                .addComponent(botonIrPer)
                .addGap(18, 18, 18)
                .addComponent(botonIrApa)
                .addGap(18, 18, 18)
                .addComponent(botonIrPad)
                .addGap(85, 85, 85)
                .addComponent(botonRecargar)
                .addGap(26, 26, 26)
                .addComponent(botonSalir)
                .addGap(116, 116, 116))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jPanelDominante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(jPanelRecesivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(174, 174, 174)
                        .addComponent(jlblpad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSliderPadPer, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblPer)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(botonRecargar)
                        .addComponent(botonSalir))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(botonIrApa)
                        .addComponent(botonIrPad)
                        .addComponent(botonIrPer)))
                .addContainerGap(124, Short.MAX_VALUE))
        );

        jSliderPadPer.getAccessibleContext().setAccessibleName("");
        jSliderPadPer.getAccessibleContext().setAccessibleDescription("");

        pack();
        
        
    }
    
    
    
    public static VistaGraficaPadrinoPerro getInstancia() {
    if (instancia == null) {
       instancia = new VistaGraficaPadrinoPerro(); 
    } 
    return instancia;
    }

    /**
     * @return the jPanelDominante
     */
    public JPanel getPanelDominante() {
        return jPanelDominante;
    }

    /**
     * @return the jTDominante
     */
    public JTable getTDominante() {
        return jTDominante;
    }

    /**
     * @return the botonIrPad
     */
    public JButton getBotonIrPad() {
        return botonIrPad;
    }

    /**
     * @param botonIrPad the botonIrPad to set
     */
    public void setBotonIrPad(JButton botonIrPad) {
        this.botonIrPad = botonIrPad;
    }

    /**
     * @return the jPanelRecesivo
     */
    public JPanel getPanelRecesivo() {
        return jPanelRecesivo;
    }

    /**
     * @param jPanelRecesivo the jPanelRecesivo to set
     */
    public void setPanelRecesivo(JPanel jPanelRecesivo) {
        this.jPanelRecesivo = jPanelRecesivo;
    }

    /**
     * @return the jTRecesivo
     */
    public JTable getTRecesivo() {
        return jTRecesivo;
    }

    /**
     * @param jTRecesivo the jTRecesivo to set
     */
    public void setTRecesivo(JTable jTRecesivo) {
        this.jTRecesivo = jTRecesivo;
    }

    /**
     * @return the botonIrPer
     */
    public JButton getBotonIrPer() {
        return botonIrPer;
    }

    /**
     * @param botonIrPer the botonIrPer to set
     */
    public void setBotonIrPer(JButton botonIrPer) {
        this.botonIrPer = botonIrPer;
    }

    /**
     * @return the jSliderPadPer
     */
    public JSlider getSliderPadPer() {
        return jSliderPadPer;
    }

    /**
     * @param jSliderPadPer the jSliderPadPer to set
     */
    public void setSliderPadPer(JSlider jSliderPadPer) {
        this.jSliderPadPer = jSliderPadPer;
    }

    /**
     * @param jLblPer the jLblPer to set
     */
    public void setjLblPer(JLabel jLblPer) {
        this.jLblPer = jLblPer;
    }

    /**
     * @param jlblpad the jlblpad to set
     */
    public void setJlblpad(JLabel jlblpad) {
        this.jlblpad = jlblpad;
    }

    /**
     * @return the botonRecargar
     */
    public JButton getBotonRecargar() {
        return botonRecargar;
    }

    /**
     * @return the botonSalir
     */
    public JButton getBotonSalir() {
        return botonSalir;
    }

    /**
     * @return the botonIrApa
     */
    public JButton getBotonIrApa() {
        return botonIrApa;
    }
}
