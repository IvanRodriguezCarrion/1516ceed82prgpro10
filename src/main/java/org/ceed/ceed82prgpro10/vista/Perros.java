/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceed.ceed82prgpro10.vista;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author BlackBana
 */
@Entity
@Table(name = "perros", catalog = "1516ceed82prg", schema = "")
@NamedQueries({
    @NamedQuery(name = "Perros.findAll", query = "SELECT p FROM Perros p"),
    @NamedQuery(name = "Perros.findByIdperro", query = "SELECT p FROM Perros p WHERE p.idperro = :idperro"),
    @NamedQuery(name = "Perros.findByNombreperro", query = "SELECT p FROM Perros p WHERE p.nombreperro = :nombreperro"),
    @NamedQuery(name = "Perros.findByNchip", query = "SELECT p FROM Perros p WHERE p.nchip = :nchip"),
    @NamedQuery(name = "Perros.findByRaza", query = "SELECT p FROM Perros p WHERE p.raza = :raza")})
public class Perros implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idperro")
    private Integer idperro;
    @Column(name = "nombreperro")
    private String nombreperro;
    @Column(name = "nchip")
    private BigInteger nchip;
    @Column(name = "raza")
    private String raza;

    public Perros() {
    }

    public Perros(Integer idperro) {
	this.idperro = idperro;
    }

    public Integer getIdperro() {
	return idperro;
    }

    public void setIdperro(Integer idperro) {
	Integer oldIdperro = this.idperro;
	this.idperro = idperro;
	changeSupport.firePropertyChange("idperro", oldIdperro, idperro);
    }

    public String getNombreperro() {
	return nombreperro;
    }

    public void setNombreperro(String nombreperro) {
	String oldNombreperro = this.nombreperro;
	this.nombreperro = nombreperro;
	changeSupport.firePropertyChange("nombreperro", oldNombreperro, nombreperro);
    }

    public BigInteger getNchip() {
	return nchip;
    }

    public void setNchip(BigInteger nchip) {
	BigInteger oldNchip = this.nchip;
	this.nchip = nchip;
	changeSupport.firePropertyChange("nchip", oldNchip, nchip);
    }

    public String getRaza() {
	return raza;
    }

    public void setRaza(String raza) {
	String oldRaza = this.raza;
	this.raza = raza;
	changeSupport.firePropertyChange("raza", oldRaza, raza);
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (idperro != null ? idperro.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Perros)) {
	    return false;
	}
	Perros other = (Perros) object;
	if ((this.idperro == null && other.idperro != null) || (this.idperro != null && !this.idperro.equals(other.idperro))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "org.ceed.ceed82prgpro10.vista.Perros[ idperro=" + idperro + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.removePropertyChangeListener(listener);
    }
    
}
