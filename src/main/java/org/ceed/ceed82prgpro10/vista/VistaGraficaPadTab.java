package org.ceed.ceed82prgpro10.vista;

import java.awt.Window;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.RollbackException;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class VistaGraficaPadTab extends JPanel implements ActionListener {
    private javax.swing.JButton botonBorrar;
    private javax.swing.JButton botonCrear;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton botonRecargar;
    private javax.swing.JLabel emailLabel;
    private javax.persistence.EntityManager entityManager;
    private javax.swing.JLabel idpersonaLabel;
    private java.util.List<org.ceed.ceed82prgpro10.vista.Padrinos> list;
    private javax.swing.JScrollPane masterScrollPane;
    private javax.swing.JTable masterTable;
    private javax.swing.JLabel nombrepersonaLabel;
    private javax.persistence.Query query;
    private javax.swing.JLabel telefonoLabel;
    private javax.swing.JTextField textFieldEmail;
    private javax.swing.JTextField textFieldIdPad;
    private javax.swing.JTextField textFieldNombre;
    private javax.swing.JTextField textFieldTelefono;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    private javax.swing.JButton botonSalir;    
//    private static VistaGraficaPadTab instancia;
    
    public VistaGraficaPadTab() {	
	bindingGroup = new org.jdesktop.beansbinding.BindingGroup();
        entityManager = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory("1516ceed82prg?zeroDateTimeBehavior=convertToNullPU").createEntityManager();
        query = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery("SELECT p FROM Padrinos p");
        list = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(query.getResultList());
        masterScrollPane = new javax.swing.JScrollPane();
        masterTable = new javax.swing.JTable();
        idpersonaLabel = new javax.swing.JLabel();
        nombrepersonaLabel = new javax.swing.JLabel();
        telefonoLabel = new javax.swing.JLabel();
        emailLabel = new javax.swing.JLabel();
        textFieldIdPad = new javax.swing.JTextField();
        textFieldNombre = new javax.swing.JTextField();
        textFieldTelefono = new javax.swing.JTextField();
        textFieldEmail = new javax.swing.JTextField();
        botonGuardar = new javax.swing.JButton();
        botonRecargar = new javax.swing.JButton();
        botonCrear = new javax.swing.JButton();
        botonBorrar = new javax.swing.JButton();
        botonSalir = new javax.swing.JButton();
	if (!Beans.isDesignTime()) {
	    entityManager.getTransaction().begin();
	}
	
	
        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, list, masterTable);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${idpersona}"));
        columnBinding.setColumnName("Idpersona");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nombrepersona}"));
        columnBinding.setColumnName("Nombrepersona");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${telefono}"));
        columnBinding.setColumnName("Telefono");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${email}"));
        columnBinding.setColumnName("Email");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);

        masterScrollPane.setViewportView(masterTable);

        idpersonaLabel.setText("Identificador:");

        nombrepersonaLabel.setText("Nombre:");

        telefonoLabel.setText("Telefono:");

        emailLabel.setText("Email:");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.idpersona}"), textFieldIdPad, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Escriba aquí el identificador");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), textFieldIdPad, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.nombrepersona}"), textFieldNombre, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Escriba aquí el nombre del padrino");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), textFieldNombre, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.telefono}"), textFieldTelefono, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Escriba aquí el teléfono");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), textFieldTelefono, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.email}"), textFieldEmail, org.jdesktop.beansbinding.BeanProperty.create("text"));
        binding.setSourceUnreadableValue("Escriba aquí el mail en formato mail@mail.dominio");
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), textFieldEmail, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        botonGuardar.setText("Guardar");
        botonGuardar.addActionListener(this);

        botonRecargar.setText("Recargar");
        botonRecargar.addActionListener(this);

        botonCrear.setText("Crear");
        botonCrear.addActionListener(this);

        botonBorrar.setText("Borrar");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), botonBorrar, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        botonBorrar.addActionListener(this);
	
	botonSalir.setText("Salir");
	botonSalir.addActionListener(this);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(botonSalir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                        .addComponent(botonCrear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonBorrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonRecargar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonGuardar))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(idpersonaLabel)
                                    .addComponent(nombrepersonaLabel)
                                    .addComponent(telefonoLabel)
                                    .addComponent(emailLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textFieldIdPad, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                                    .addComponent(textFieldNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                                    .addComponent(textFieldTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                                    .addComponent(textFieldEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)))
                            .addComponent(masterScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {botonBorrar, botonCrear, botonGuardar, botonRecargar});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(masterScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idpersonaLabel)
                    .addComponent(textFieldIdPad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombrepersonaLabel)
                    .addComponent(textFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telefonoLabel)
                    .addComponent(textFieldTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emailLabel)
                    .addComponent(textFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonGuardar)
                    .addComponent(botonRecargar)
                    .addComponent(botonBorrar)
                    .addComponent(botonCrear)
		    .addComponent(botonSalir))
                .addContainerGap())
        );

        bindingGroup.bind();
    }
   
    
    @Override
    public void actionPerformed(java.awt.event.ActionEvent evt) {
            if (evt.getSource() == botonGuardar) {
                botonGuardarActionPerformed(evt);
            }
            else if (evt.getSource() == botonRecargar) {
                botonRecargarActionPerformed(evt);
            }
            else if (evt.getSource() == botonCrear) {
                botonCrearActionPerformed(evt);
            }
            else if (evt.getSource() == botonBorrar) {
                botonBorrarActionPerformed(evt);
            }
	    else if (evt.getSource() == botonSalir) {
                botonSalirActionPerformed(evt);
            }
        }
    
    private void botonRecargarActionPerformed(java.awt.event.ActionEvent evt) {                                              
	entityManager.getTransaction().rollback();
	entityManager.getTransaction().begin();
	java.util.Collection data = query.getResultList();
	for (Object entity : data) {
	    entityManager.refresh(entity);
	}
	list.clear();
	list.addAll(data);
    }                                             

    private void botonBorrarActionPerformed(java.awt.event.ActionEvent evt) {                                            
	int[] selected = masterTable.getSelectedRows();
	List<org.ceed.ceed82prgpro10.vista.Padrinos> toRemove = new ArrayList<org.ceed.ceed82prgpro10.vista.Padrinos>(selected.length);
	for (int idx = 0; idx < selected.length; idx++) {
	    org.ceed.ceed82prgpro10.vista.Padrinos p = list.get(masterTable.convertRowIndexToModel(selected[idx]));
	    toRemove.add(p);
	    entityManager.remove(p);
	}
	list.removeAll(toRemove);
    }                                           

    private void botonCrearActionPerformed(java.awt.event.ActionEvent evt) {                                           
	org.ceed.ceed82prgpro10.vista.Padrinos p = new org.ceed.ceed82prgpro10.vista.Padrinos();
	entityManager.persist(p);
	list.add(p);
	int row = list.size() - 1;
	masterTable.setRowSelectionInterval(row, row);
	masterTable.scrollRectToVisible(masterTable.getCellRect(row, 0, true));
    }                                          
    
    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {                                             
	try {
	    entityManager.getTransaction().commit();
	    entityManager.getTransaction().begin();
	} catch (RollbackException rex) {
	    rex.printStackTrace();
	    entityManager.getTransaction().begin();
	    List<org.ceed.ceed82prgpro10.vista.Padrinos> merged = new ArrayList<org.ceed.ceed82prgpro10.vista.Padrinos>(list.size());
	    for (org.ceed.ceed82prgpro10.vista.Padrinos p : list) {
		merged.add(entityManager.merge(p));
	    }
	    list.clear();
	    list.addAll(merged);
	}
    }                
    
    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {                                             
	 Window w = SwingUtilities.getWindowAncestor(this);
	 w.setVisible(false);
    }                                         
    
    public javax.swing.JButton getSalirPadTab(){
	return botonSalir;
    }
//    public static VistaGraficaPadTab getInstancia() {
//        if (instancia == null) {
//           instancia = new VistaGraficaPadTab(); 
//        } 
//        return instancia;
//    }
}