package org.ceed.ceed82prgpro10.vista;
import java.awt.FlowLayout; // Gestor de contenido tipo Flow
import java.awt.GridLayout; // Gestor de contenido tipo Grid.
import javax.swing.UIManager;
import javax.swing.JFrame; // Poder crear J.Frames
import javax.swing.JPanel; // Para crear paneles
import javax.swing.JLabel; // Para crear textos
import javax.swing.JButton; // Para crear botones
import javax.swing.WindowConstants;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon; // Permite insertar iconos.
import javax.swing.JDesktopPane;
import java.awt.Color;
import java.awt.Dimension;





public class VistaGrafica extends JFrame{
    
    private JDesktopPane jDesktopGlobal = new JDesktopPane();
    private JMenuBar menuPrincipal = new JMenuBar();
    private JMenu menuPad = new JMenu();
    private JMenu menuPer= new JMenu();
    private JMenu menuSal = new JMenu();
    private JMenu menuAc = new JMenu();
    private JMenu menuApa  = new JMenu();
    private JMenu menuBD = new JMenu();
    private JMenu menuDocu = new JMenu();
    private JMenuItem menuItemAce = new JMenuItem();
    private JMenuItem menuItemApa = new JMenuItem();
    private JMenuItem menuItemBDCrear = new JMenuItem();
    private JMenuItem menuItemBDInsert = new JMenuItem();
    private JMenuItem menuItemBDInst = new JMenuItem();
    private JMenuItem menuItemDocuOn = new JMenuItem();
    private JMenuItem menuItemDocuPDF = new JMenuItem();
    private JMenuItem menuItemPad = new JMenuItem();
    private JMenuItem menuItemPe = new JMenuItem();
    private JMenuItem menuItemSal = new JMenuItem();
    private JMenuItem menuItemPadPer = new JMenuItem();
    private JMenuItem menuItemPerPad = new JMenuItem();
    private JMenuItem menuItemPadTab = new JMenuItem();
    private JMenuItem menuItemPerTab = new JMenuItem();
    private JLabel lblTitulo = new JLabel();
    private JLabel lblportperr = new JLabel();
    

    public void menuGrafico () {
        setTitle("Sistema de gestión de apadrinamientos 2015-2016 por Ringo S.L");
            
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        jDesktopGlobal.setBackground(new Color(204, 204, 204));
        jDesktopGlobal.setPreferredSize(new Dimension(800, 550));
        
        lblTitulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/titulo.png"))); // NOI18N

        lblportperr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/portada.png"))); // NOI18N

        jDesktopGlobal.setLayer(lblTitulo, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopGlobal.setLayer(lblportperr, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopGlobalLayout = new javax.swing.GroupLayout(jDesktopGlobal);
        jDesktopGlobal.setLayout(jDesktopGlobalLayout);
        jDesktopGlobalLayout.setHorizontalGroup(
            jDesktopGlobalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopGlobalLayout.createSequentialGroup()
                .addGroup(jDesktopGlobalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDesktopGlobalLayout.createSequentialGroup()
                        .addGap(303, 303, 303)
                        .addComponent(lblportperr))
                    .addGroup(jDesktopGlobalLayout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addComponent(lblTitulo)))
                .addContainerGap(181, Short.MAX_VALUE))
        );
        jDesktopGlobalLayout.setVerticalGroup(
            jDesktopGlobalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopGlobalLayout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addComponent(lblTitulo)
                .addGap(107, 107, 107)
                .addComponent(lblportperr)
                .addContainerGap(170, Short.MAX_VALUE))
        );

        menuPrincipal.setPreferredSize(new java.awt.Dimension(800, 50));

        menuPad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/padrino.png"))); // NOI18N
        menuPad.setText("Padrinos");

        menuItemPad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pad.png"))); // NOI18N
        menuItemPad.setText("Gestión de Padrinos");
        
        menuPad.add(menuItemPad);
        
        menuItemPadPer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bdtabla.png"))); // NOI18N
        menuItemPadPer.setText("Relación Padrino-Perro");

        menuPad.add(menuItemPadPer);
	
	menuItemPadTab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/padtab.png"))); // NOI18N
	menuItemPadTab.setText("Tabla de Padrinos");
	
	menuPad.add(menuItemPadTab);
	
        menuPrincipal.add(menuPad);

        menuPer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/perro.png"))); // NOI18N
        menuPer.setText("Perros");

        menuItemPe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/per.png"))); // NOI18N
        menuItemPe.setText("Gestión de Perros");
        menuPer.add(menuItemPe);
        
        menuItemPerPad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bdtabla.png"))); // NOI18N
        menuItemPerPad.setText("Relación Perro-Padrino");

        menuPer.add(menuItemPerPad);
	
	menuItemPerTab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pertab.png"))); // NOI18N
	menuItemPerTab.setText("Tabla de Perros");
	
	menuPer.add(menuItemPerTab);

        menuPrincipal.add(menuPer);

        menuApa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/apadrina.png"))); // NOI18N
        menuApa.setText("Apadrinamiento");

        menuItemApa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/apa.png"))); // NOI18N
        menuItemApa.setText("Gestión de Apadrinamientos");
        menuApa.add(menuItemApa);

        menuPrincipal.add(menuApa);

        menuBD.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bd.png"))); // NOI18N
        menuBD.setText("Base de datos");

        menuItemBDInst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bdinstall.png"))); // NOI18N
        menuItemBDInst.setText("Instalar la Base de Datos");
        
        menuBD.add(menuItemBDInst);

        menuItemBDInsert.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bdinsert.png"))); // NOI18N
        menuItemBDInsert.setText("Desinstalar la Base de Datos");
        
        menuBD.add(menuItemBDInsert);

        menuPrincipal.add(menuBD);

        menuDocu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/documentacion.png"))); // NOI18N
        menuDocu.setText("Documentación");

        menuItemDocuPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/docpdf.png"))); // NOI18N
        menuItemDocuPDF.setText("Ver en PDF");
        menuDocu.add(menuItemDocuPDF);

        menuItemDocuOn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/docuweb.png"))); // NOI18N
        menuItemDocuOn.setText("Ver online");
        menuDocu.add(menuItemDocuOn);

        menuPrincipal.add(menuDocu);

        menuAc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/acercade.png"))); // NOI18N
        menuAc.setText("Acerca de");

        menuItemAce.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/acerca.png"))); // NOI18N
        menuItemAce.setText("¿Desea saber más?");
        menuAc.add(menuItemAce);

        menuPrincipal.add(menuAc);

        menuSal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconosalir.png"))); // NOI18N
        menuSal.setText("Salir");

        menuItemSal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/sal.png"))); // NOI18N
        menuItemSal.setText("Cerrar el programa");
        menuSal.add(menuItemSal);

        menuPrincipal.add(menuSal);

        setJMenuBar(menuPrincipal);
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopGlobal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopGlobal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        
        setVisible(true);
        
        pack(); 
    }
    

        
    /*Estos gets nos permiten recuperar los botones en otras clases, 
    ya que los botones son variables de clase y DEBEN ser privadas*/
    
    //getters para el menu principal.
    
     
    public JMenuItem getBotonPadrino(){
        return menuItemPad;
        } 
    public JMenuItem getBotonPerro(){
        return menuItemPe;
        }  
     public JMenuItem getBotonApadrinamiento(){
        return menuItemApa;
        }    
     public JMenuItem getBotonBdInst(){
        return menuItemBDInst;
        }    
     public JMenuItem getBotonBdTab(){
        return menuItemBDCrear;
        }
     public JMenuItem getBotonBdInsert(){
        return menuItemBDInsert;
        }         
    public JMenuItem getBotonDocuPDF(){
        return menuItemDocuPDF;
        }
    public JMenuItem getBotonDocuOnl(){
        return menuItemDocuOn;
        }      
    public JMenuItem getBotonSalir(){
        return menuItemSal;
        }  
    public JMenuItem getBotonAcercade(){
        return menuItemAce;
        }
    public JMenuItem getBotonPadPer(){
        return menuItemPadPer;
        }
    public JMenuItem getBotonPerPad(){
        return menuItemPerPad;
        }
    public JDesktopPane getEscritorio(){
        return jDesktopGlobal;
        }
    public JMenuItem getBotonPadTab() {
	return menuItemPadTab;
    }
     public JMenuItem getBotonPerTab() {
	return menuItemPerTab;
    }

}

