/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceed.ceed82prgpro10.vista;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author BlackBana
 */
@Entity
@Table(name = "padrinos", catalog = "1516ceed82prg", schema = "")
@NamedQueries({
    @NamedQuery(name = "Padrinos.findAll", query = "SELECT p FROM Padrinos p"),
    @NamedQuery(name = "Padrinos.findByIdpersona", query = "SELECT p FROM Padrinos p WHERE p.idpersona = :idpersona"),
    @NamedQuery(name = "Padrinos.findByNombrepersona", query = "SELECT p FROM Padrinos p WHERE p.nombrepersona = :nombrepersona"),
    @NamedQuery(name = "Padrinos.findByTelefono", query = "SELECT p FROM Padrinos p WHERE p.telefono = :telefono"),
    @NamedQuery(name = "Padrinos.findByEmail", query = "SELECT p FROM Padrinos p WHERE p.email = :email")})
public class Padrinos implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idpersona")
    private Integer idpersona;
    @Column(name = "nombrepersona")
    private String nombrepersona;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "email")
    private String email;

    public Padrinos() {
    }

    public Padrinos(Integer idpersona) {
	this.idpersona = idpersona;
    }

    public Integer getIdpersona() {
	return idpersona;
    }

    public void setIdpersona(Integer idpersona) {
	Integer oldIdpersona = this.idpersona;
	this.idpersona = idpersona;
	changeSupport.firePropertyChange("idpersona", oldIdpersona, idpersona);
    }

    public String getNombrepersona() {
	return nombrepersona;
    }

    public void setNombrepersona(String nombrepersona) {
	String oldNombrepersona = this.nombrepersona;
	this.nombrepersona = nombrepersona;
	changeSupport.firePropertyChange("nombrepersona", oldNombrepersona, nombrepersona);
    }

    public String getTelefono() {
	return telefono;
    }

    public void setTelefono(String telefono) {
	String oldTelefono = this.telefono;
	this.telefono = telefono;
	changeSupport.firePropertyChange("telefono", oldTelefono, telefono);
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	String oldEmail = this.email;
	this.email = email;
	changeSupport.firePropertyChange("email", oldEmail, email);
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (idpersona != null ? idpersona.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Padrinos)) {
	    return false;
	}
	Padrinos other = (Padrinos) object;
	if ((this.idpersona == null && other.idpersona != null) || (this.idpersona != null && !this.idpersona.equals(other.idpersona))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "org.ceed.ceed82prgpro10.vista.Padrinos[ idpersona=" + idpersona + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.removePropertyChangeListener(listener);
    }
    
}
