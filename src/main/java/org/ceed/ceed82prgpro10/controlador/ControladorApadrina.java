package org.ceed.ceed82prgpro10.controlador;

import org.ceed.ceed82prgpro10.modelo.Padrinos;
import org.ceed.ceed82prgpro10.modelo.Perros;
import org.ceed.ceed82prgpro10.modelo.Apadrina;
import org.ceed.ceed82prgpro10.modelo.IModelo;
import org.ceed.ceed82prgpro10.vista.VistaGraficaApadrina;
import org.ceed.ceed82prgpro10.vista.VistaGrafica;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import org.ceed.ceed82prgpro10.vista.Funciones;

/**
 ** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
 */
public class ControladorApadrina implements ActionListener, MouseListener, ItemListener, FocusListener {

    private IModelo imodelo;
    private VistaGrafica vistagrafica = new VistaGrafica();
    private VistaGraficaApadrina vistagraficapadrina;
    private Padrinos padrino = new Padrinos();
    private Perros perro = new Perros();
    private Apadrina apadrina = new Apadrina();
    private Apadrina apadrina_indice = new Apadrina();
    private Funciones f = new Funciones();
    private String opcionEscogida;
    private boolean no_actualizar, no_actualizar_perros, no_actualizar_padrinos;
    private int posicion;
    private ArrayList padrinos, perros, apadrinas;
    private StringTokenizer stokenizer;

    public ControladorApadrina(IModelo m, VistaGraficaApadrina v) {
	vistagraficapadrina = v;
	imodelo = m;
	//Con estos escondemos aquello que sólo aparecerá con el read, update y delete
	activarDesactivarBotones(true);
	activarDesactivarCamposTexto(false);
	activarDesactivarLeer(false);
	activarDesactivarAceptarCancelar(false);
	camposSoloLectura();
	camposOcultos();
	// Vigilamos los eventos sobre los botones
	actionListenerBotones();
	//Vigilamos los eventos sobre las flechas del cambio de combobox.
	mouseListenerBotones();
	//Vigilamos los eventos sobre el combobox mediante ItemListener
	itemListenerCB();

	//Vigilamos que la IDApadrina no se repita ni se quede vacía
	vistagraficapadrina.getTextFieldIdApadrina().addFocusListener(this);
    }
    
    //Código importante.
    private void create() {
	Apadrina apadrina = obtenerApadrinamiento();
	imodelo.create(apadrina);
    }

    private void read() {
	ArrayList ArrayListIteratorLeer = imodelo.readApadrina();
	Iterator apadrinas_iterator_leer = ArrayListIteratorLeer.iterator();
	while (apadrinas_iterator_leer.hasNext()) {
	    apadrina = (Apadrina) apadrinas_iterator_leer.next();
	    int id = apadrina.getIdapadrina();
	    String nombre = apadrina.getPadrinos().getNombrepersona();
	    String nombre_perro = apadrina.getPerros().getNombreperro();
	    vistagraficapadrina.getComboLeer().addItem(id + " - " + nombre + " - " + nombre_perro);
	}
    }

    private void update() {
	Apadrina apadrina = obtenerApadrinamiento();
	imodelo.update(apadrina);
    }

    private void delete() {
	Apadrina apadrina = obtenerApadrinamiento();
	imodelo.delete(apadrina);
    }

//Performances en general.
    @Override
    public void actionPerformed(ActionEvent event) { // Metodos implementados por ActionListener que vigila los botones
	if (vistagraficapadrina.getCreate() == event.getSource()) {
	    botonCrear();
	} else if (vistagraficapadrina.getRead() == event.getSource()) {
	    botonLeer();
	} else if (vistagraficapadrina.getUpdate() == event.getSource()) {
	    botonActualizar();
	} else if (vistagraficapadrina.getDelete() == event.getSource()) {
	    botonBorrar();
	} else if (vistagraficapadrina.getExit() == event.getSource()) {
	    vistagraficapadrina.dispose();
	} else if (vistagraficapadrina.getAceptar() == event.getSource()) {
	    botonAceptar();
	} else if (vistagraficapadrina.getLimpiar() == event.getSource()) {
	    limpiar();
	} else if (vistagraficapadrina.getCancelar() == event.getSource()) {
	    cancelarTodo();
	}
    }
    
 @Override
    public void mouseClicked(MouseEvent e) {
	if (vistagraficapadrina.getBotonIzq() == e.getSource()) {
	    anterior();
	} else if (vistagraficapadrina.getBotonDer() == e.getSource()) {
	    siguiente();
	} else if (vistagraficapadrina.getBotonPri() == e.getSource()) {
	    primer();
	} else if (vistagraficapadrina.getBotonUlt() == e.getSource()) {
	    ulti();
	}
    }
    
// Todo lo relacionado con Listeners
    private void actionListenerBotones() {
	vistagraficapadrina.getCreate().addActionListener(this);
	vistagraficapadrina.getRead().addActionListener(this);
	vistagraficapadrina.getUpdate().addActionListener(this);
	vistagraficapadrina.getDelete().addActionListener(this);
	vistagraficapadrina.getCancelar().addActionListener(this);
	vistagraficapadrina.getLimpiar().addActionListener(this);
	vistagraficapadrina.getExit().addActionListener(this);
	vistagraficapadrina.getAceptar().addActionListener(this);
	vistagraficapadrina.getComprobarId().addActionListener(this);
    }

    private void mouseListenerBotones() {
	vistagraficapadrina.getBotonIzq().addMouseListener(this);
	vistagraficapadrina.getBotonDer().addMouseListener(this);
	vistagraficapadrina.getBotonPri().addMouseListener(this);
	vistagraficapadrina.getBotonUlt().addMouseListener(this);
    }

    private void itemListenerCB() {
	vistagraficapadrina.getComboLeer().addItemListener(this);
	vistagraficapadrina.getComboLeerPadrinos().addItemListener(this);
	vistagraficapadrina.getComboLeerPerros().addItemListener(this);
    }

    //Llenado de las cajas de texto automático.
    private void llenadoTextBoxConComboBoxApadrina() {
	ArrayList ArrayIterator = imodelo.readApadrina();
	Iterator apadrinas = ArrayIterator.iterator();
	Apadrina apadrina_iterator;
	while (apadrinas.hasNext()) {

	    apadrina_iterator = (Apadrina) apadrinas.next();
	    String ComboBox = (String) vistagraficapadrina.getComboLeer().getSelectedItem();
	    stokenizer = new StringTokenizer(ComboBox, " - ");
	    int idComboBox = Integer.parseInt(stokenizer.nextToken());
	    if (idComboBox == apadrina_iterator.getIdapadrina()) {
		apadrina = apadrina_iterator;
	    }
	}
	settearCajasIdFecha(apadrina);
    }
    
    
    //Funciones generales.
    private void settearCajasIdFecha(Apadrina apa) {
	vistagraficapadrina.getTextFieldIdApadrina().setText(Integer.toString(apa.getIdapadrina()));
	vistagraficapadrina.getTextFieldIdPersona().setText(Integer.toString(apa.getPadrinos().getIdpersona()));
	vistagraficapadrina.getTextFieldIdPerro().setText(Integer.toString(apa.getPerros().getIdperro()));
	vistagraficapadrina.getBotonFecha().setDate(apa.getFechaapa());
    }
    
    // Llenado de comboboxes
    private void llenadoComboBoxPadrinoPerro() {
	//Llenado ComboBox Perro
	ArrayList ArrayListIteratorLeerPerros = imodelo.readPerro();
	Iterator perros_iterator_leer = ArrayListIteratorLeerPerros.iterator();
	while (perros_iterator_leer.hasNext()) {
	    perro = (Perros) perros_iterator_leer.next();
	    int id = perro.getIdperro();
	    String nombre = perro.getNombreperro();
	    vistagraficapadrina.getComboLeerPerros().addItem(id + " - " + nombre);
	}
	//Llenado ComboBox Padrinos
	ArrayList ArrayListIteratorLeerPadrinos = imodelo.readPadrino();
	Iterator padrinos_iterator_leer = ArrayListIteratorLeerPadrinos.iterator();
	while (padrinos_iterator_leer.hasNext()) {
	    padrino = (Padrinos) padrinos_iterator_leer.next();
	    int id = padrino.getIdpersona();
	    String nombre = padrino.getNombrepersona();
	    vistagraficapadrina.getComboLeerPadrinos().addItem(id + " - " + nombre);
	}
    }

    //Funciones para activar o desactivar funcionalidades
    private void camposOcultos() {
	vistagraficapadrina.getTextFieldIdPersona().setVisible(false);
	vistagraficapadrina.getTextFieldIdPerro().setVisible(false);
	vistagraficapadrina.getComprobarId().setVisible(false);
	vistagraficapadrina.getTextFieldIdApadrina().setVisible(false);
	vistagraficapadrina.getLblIdApadrina().setVisible(false);
    }
    
    private void activarDesactivarBotones(boolean unouotro) {
	vistagraficapadrina.getCreate().setEnabled(unouotro);
	vistagraficapadrina.getRead().setEnabled(unouotro);
	vistagraficapadrina.getUpdate().setEnabled(unouotro);
	vistagraficapadrina.getDelete().setEnabled(unouotro);
    }

    private void activarDesactivarLeer(boolean activoono) {
	vistagraficapadrina.getComboLeer().setVisible(activoono);
	vistagraficapadrina.getBotonIzq().setVisible(activoono);
	vistagraficapadrina.getBotonDer().setVisible(activoono);
	vistagraficapadrina.getBotonPri().setVisible(activoono);
	vistagraficapadrina.getBotonUlt().setVisible(activoono);
    }

    private void activarDesactivarCamposTexto(boolean activoono) {
	vistagraficapadrina.getTextFieldIdPersona().setEditable(activoono);
	vistagraficapadrina.getTextFieldIdPerro().setEditable(activoono);
	vistagraficapadrina.getTextFieldIdApadrina().setEditable(activoono);
    }

    private void activarUpdateDelete(boolean activoono) {
	vistagraficapadrina.getComboLeerPadrinos().setVisible(activoono);
	vistagraficapadrina.getComboLeerPerros().setVisible(activoono);
	vistagraficapadrina.getComboLeer().setVisible(activoono);
	vistagraficapadrina.getBotonIzq().setVisible(activoono);
	vistagraficapadrina.getBotonDer().setVisible(activoono);
	vistagraficapadrina.getBotonPri().setVisible(activoono);
	vistagraficapadrina.getBotonUlt().setVisible(activoono);
	vistagraficapadrina.getTextFieldIdApadrina().setEditable(false);
	vistagraficapadrina.getTextFieldIdPersona().setEditable(activoono);
	vistagraficapadrina.getTextFieldIdPerro().setEditable(activoono);
    }

    private void vaciarTodosCampos() {
	no_actualizar = false;
	no_actualizar_perros = false;
	no_actualizar_padrinos = false;
	vistagraficapadrina.getComboLeer().removeAllItems();
	vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
	vistagraficapadrina.getComboLeerPerros().removeAllItems();
	no_actualizar_perros = true;
	no_actualizar_padrinos = true;
	no_actualizar = true;
	limpiar();
    }

    private void activarDesactivarAceptarCancelar(boolean eleccion) {
	vistagraficapadrina.getAceptar().setEnabled(eleccion);
	vistagraficapadrina.getCancelar().setEnabled(eleccion);
	vistagraficapadrina.getLimpiar().setEnabled(eleccion);
    }

    //Devuelve la aplicación a su estado original.
    private void cancelarTodo() {
	activarDesactivarBotones(true);
	activarDesactivarAceptarCancelar(false);
	vistagraficapadrina.getAceptar().setVisible(true);
	activarDesactivarCamposTexto(false);
	activarDesactivarLeer(false);
	vaciarTodosCampos();
	vistagraficapadrina.getTextFieldIdApadrina().setVisible(false);
	vistagraficapadrina.getLblIdApadrina().setVisible(false);
    }

    private void limpiar() {
	vistagraficapadrina.getTextFieldIdApadrina().setText("");
	vistagraficapadrina.getTextFieldNombre().setText("");
	vistagraficapadrina.getTextFieldTelefono().setText("");
	vistagraficapadrina.getTextFieldMascota().setText("");
	vistagraficapadrina.getTextFieldNChip().setText("");
    }

    private void camposSoloLectura() {
	vistagraficapadrina.getTextFieldNombre().setEditable(false);
	vistagraficapadrina.getTextFieldMascota().setEditable(false);
	vistagraficapadrina.getTextFieldNChip().setEditable(false);
	vistagraficapadrina.getTextFieldTelefono().setEditable(false);
    }
    
    private void activarDesactivarEventosActualizacionCB(boolean eleccion) {
	no_actualizar_perros = eleccion;
	no_actualizar_padrinos = eleccion;
	no_actualizar = eleccion;
    }    
    
// Manejo de los botones para moverse por el CBApadrina.
    private void anterior() {
	ArrayList arrayListMover = imodelo.readApadrina();
	Iterator apadrinas = arrayListMover.iterator();

	while (apadrinas.hasNext()) {
	    apadrina = (Apadrina) apadrinas.next();
	    int id = Integer.parseInt(vistagraficapadrina.getTextFieldIdApadrina().getText());
	    if (id == apadrina.getIdapadrina()) {
		posicion = arrayListMover.indexOf(apadrina);
	    }
	}
	if (posicion != 0) {
	    posicion -= 1;
	    apadrina_indice = ((Apadrina) arrayListMover.get(posicion));
	    recorrido(apadrina_indice, posicion);
	} else {
	    posicion = arrayListMover.size() - 1;
	    apadrina_indice = ((Apadrina) arrayListMover.get(posicion));
	    recorrido(apadrina_indice, posicion);
	}
    }

    private void siguiente() {
	ArrayList arrayListMover = imodelo.readApadrina();
	Iterator apadrinas = arrayListMover.iterator();
	while (apadrinas.hasNext()) {
	    apadrina = (Apadrina) apadrinas.next();
	    String id = vistagraficapadrina.getTextFieldIdApadrina().getText();
	    if (id.equals(apadrina.getIdapadrina())) {
		posicion = arrayListMover.indexOf(apadrina);
		recorrido(apadrina, posicion);
	    }
	}
	if (posicion != arrayListMover.size() - 1) {
	    posicion += 1;
	    apadrina_indice = ((Apadrina) arrayListMover.get(posicion));
	    recorrido(apadrina_indice, posicion);
	} else {
	    posicion = 0;
	    apadrina_indice = ((Apadrina) arrayListMover.get(posicion));
	    recorrido(apadrina_indice, posicion);
	}
    }

    private void primer() {
	ArrayList arrayListMover = imodelo.readApadrina();
	if (arrayListMover != null) {
	    posicion = 0;
	    apadrina_indice = ((Apadrina) arrayListMover.get(posicion));
	    recorrido(apadrina_indice, posicion);
	} else {
	    apadrina_indice = null;
	    posicion = -1;
	}
    }

    private void ulti() {
	ArrayList arrayListMover = imodelo.readApadrina();
	posicion = (arrayListMover.size() - 1);
	apadrina_indice = ((Apadrina) arrayListMover.get(posicion));
	recorrido(apadrina_indice, posicion);
    }

    private void recorrido(Apadrina apadrinaPuesto, int sitio) {
	vistagraficapadrina.getComboLeer().setSelectedIndex(sitio);
	vistagraficapadrina.getTextFieldIdPersona().setText(Integer.toString(apadrinaPuesto.getPadrinos().getIdpersona()));
	vistagraficapadrina.getTextFieldIdPerro().setText(Integer.toString(apadrinaPuesto.getPerros().getIdperro()));
	vistagraficapadrina.getTextFieldIdApadrina().setText(Integer.toString(apadrinaPuesto.getIdapadrina()));
	vistagraficapadrina.getBotonFecha().setDate(apadrinaPuesto.getFechaapa());
    }
 
//Botones del actionPerformance 
    private void botonCrear() {
	vistagraficapadrina.getComboLeerPerros().removeAllItems();
	vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
	vistagraficapadrina.getComboLeerPerros().setEnabled(true);
	vistagraficapadrina.getComboLeerPadrinos().setEnabled(true);
	vistagraficapadrina.getBotonFecha().setEnabled(true);
	vistagraficapadrina.getComboLeer().removeItemListener(this);
	vistagraficapadrina.getComboLeerPadrinos().removeItemListener(this);
	vistagraficapadrina.getComboLeerPerros().removeItemListener(this);
	vistagraficapadrina.getComboLeer().addItemListener(this);
	vistagraficapadrina.getComboLeerPadrinos().addItemListener(this);
	vistagraficapadrina.getComboLeerPerros().addItemListener(this);
	no_actualizar_perros = true;
	no_actualizar_padrinos = true;
	llenadoComboBoxPadrinoPerro();
	activarDesactivarAceptarCancelar(true);
	activarDesactivarBotones(false);
	activarDesactivarCamposTexto(true);
	activarDesactivarLeer(false);
	vistagraficapadrina.getTextFieldIdApadrina().setVisible(false);
	vistagraficapadrina.getLblIdApadrina().setVisible(false);
	opcionEscogida = "crear";
    }
    
    private void botonLeer() {
	vistagraficapadrina.getTextFieldIdApadrina().setVisible(true);
	vistagraficapadrina.getLblIdApadrina().setVisible(true);
	no_actualizar = false;
	no_actualizar_perros = false;
	no_actualizar_padrinos = false;
	activarDesactivarBotones(false);
	vistagraficapadrina.getComboLeerPerros().removeAllItems();
	vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
	vistagraficapadrina.getComboLeerPerros().setEnabled(false);
	vistagraficapadrina.getComboLeerPadrinos().setEnabled(false);
	vistagraficapadrina.getBotonFecha().setEnabled(false);
	llenadoComboBoxPadrinoPerro();
	activarDesactivarLeer(true);
	vistagraficapadrina.getCancelar().setEnabled(true);
	read();
	llenadoTextBoxConComboBoxApadrina();
	opcionEscogida = "leer";
	no_actualizar = true;
	no_actualizar_perros = true;
	no_actualizar_padrinos = true;
    }

    private void botonActualizar () {
	vistagraficapadrina.getTextFieldIdApadrina().setVisible(true);
	vistagraficapadrina.getLblIdApadrina().setVisible(true);
	no_actualizar_perros = false;
	no_actualizar_padrinos = false;
	no_actualizar = false;
	vistagraficapadrina.getComboLeerPerros().removeAllItems();
	vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
	vistagraficapadrina.getComboLeerPerros().setEnabled(true);
	vistagraficapadrina.getComboLeerPadrinos().setEnabled(true);
	read();
	llenadoTextBoxConComboBoxApadrina();
	llenadoComboBoxPadrinoPerro();
	activarDesactivarAceptarCancelar(true);
	activarDesactivarBotones(false);
	activarDesactivarCamposTexto(true);
	activarDesactivarLeer(true);
	vistagraficapadrina.getTextFieldIdApadrina().setEnabled(false);
	vistagraficapadrina.getBotonFecha().setEnabled(true);
	opcionEscogida = "actualizar";
	no_actualizar = true;
	no_actualizar_perros = true;
	no_actualizar_padrinos = true;
    }
    
    private void botonBorrar() {
	vistagraficapadrina.getTextFieldIdApadrina().setVisible(true);
	vistagraficapadrina.getLblIdApadrina().setVisible(true);
	no_actualizar_perros = false;
	no_actualizar_padrinos = false;
	no_actualizar = false;
	activarDesactivarEventosActualizacionCB(false);
	activarDesactivarAceptarCancelar(true);
	activarUpdateDelete(true);
	activarDesactivarBotones(false);
	vistagraficapadrina.getComboLeerPerros().removeAllItems();
	vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
	vistagraficapadrina.getComboLeerPerros().setEnabled(false);
	vistagraficapadrina.getComboLeerPadrinos().setEnabled(false);
	vistagraficapadrina.getTextFieldIdApadrina().setEnabled(false);
	vistagraficapadrina.getBotonFecha().setEnabled(false);
	read();
	llenadoTextBoxConComboBoxApadrina();
	llenadoComboBoxPadrinoPerro();
	activarDesactivarAceptarCancelar(true);
	activarDesactivarBotones(false);
	activarDesactivarCamposTexto(true);
	activarDesactivarLeer(true);
	opcionEscogida = "borrar";
	activarDesactivarEventosActualizacionCB(true);
	no_actualizar_perros = true;
	no_actualizar_padrinos = true;
	no_actualizar = true;
    }
    
     private void botonAceptar() {
	switch (opcionEscogida) {
	    case "crear":
		no_actualizar = false;
		create();
		no_actualizar = true;
		break;
	    case "actualizar":
		update();
		break;
	    case "borrar":
		delete();
		break;
	}
    }
    
    private Apadrina obtenerApadrinamiento() {
	perro = new Perros();
	padrino = new Padrinos();

	int idapa = Integer.parseInt(vistagraficapadrina.getTextFieldIdApadrina().getText());
	padrino.setIdpersona(Integer.parseInt(vistagraficapadrina.getTextFieldIdPersona().getText()));
	perro.setIdperro(Integer.parseInt(vistagraficapadrina.getTextFieldIdPerro().getText()));
	Date fecha = (Date) vistagraficapadrina.getBotonFecha().getDate();

	Apadrina apadrina = new Apadrina(idapa, padrino, perro, fecha);
	return apadrina;
    }


    @Override
    public void itemStateChanged(ItemEvent e) {
	if (vistagraficapadrina.getComboLeer() == e.getSource()) {
	    if (no_actualizar) {
		cambioCBApadrina();
	    }
	} else if (vistagraficapadrina.getComboLeerPadrinos() == e.getSource()) {
	    if (no_actualizar_padrinos) {
		cambioCBPadrinos();
	    }
	} else if (vistagraficapadrina.getComboLeerPerros() == e.getSource()) {
	    if (no_actualizar_perros) {
		cambioCBPerros();

	    }
	}
    }

    private void cambioCBApadrina() {
	int id;
	Iterator it;
	Apadrina apadrina;
	String idCombo = (String) vistagraficapadrina.getComboLeer().getSelectedItem();
	stokenizer = new StringTokenizer(idCombo, " - ");
	id = Integer.parseInt(stokenizer.nextToken());
	apadrinas = imodelo.readApadrina();
	it = apadrinas.iterator();
	while (it.hasNext()) {
	    apadrina = (Apadrina) it.next();
	    if (apadrina.getIdapadrina() == id) {
		rellenarCajasConCB(apadrina);
	    }
	}
	posicionarCBPadrinos();
	posicionarCBPerros();
    }
    
    private void posicionarCBPadrinos() {
	int recuperarIdPersona = Integer.parseInt(vistagraficapadrina.getTextFieldIdPersona().getText());
	ArrayList arrayListBuscarIdPadrino = imodelo.readPadrino();
	Iterator padrinos = arrayListBuscarIdPadrino.iterator();

	while (padrinos.hasNext()) {
	    padrino = (Padrinos) padrinos.next();
	    if (recuperarIdPersona == padrino.getIdpersona()) {
		int posicion_padrino = arrayListBuscarIdPadrino.indexOf(padrino);
		vistagraficapadrina.getComboLeerPadrinos().setSelectedIndex(posicion_padrino);
	    }
	}
    }
    
    private void posicionarCBPerros() {
	int recuperarIdPerro = Integer.parseInt(vistagraficapadrina.getTextFieldIdPerro().getText());
	ArrayList arrayListBuscarIdPerro = imodelo.readPerro();
	Iterator perros = arrayListBuscarIdPerro.iterator();

	while (perros.hasNext()) {
	    perro = (Perros) perros.next();
	    if (recuperarIdPerro == perro.getIdperro()) {
		int posicion_perro = arrayListBuscarIdPerro.indexOf(perro);
		vistagraficapadrina.getComboLeerPerros().setSelectedIndex(posicion_perro);
	    }
	}
    }
    
    private void rellenarCajasConCB(Apadrina apa) {
	vistagraficapadrina.getTextFieldIdApadrina().setText(Integer.toString(apa.getIdapadrina()));
	vistagraficapadrina.getTextFieldMascota().setText(apa.getPerros().getNombreperro());
	vistagraficapadrina.getTextFieldNChip().setText(Long.toString(apa.getPerros().getNchip()));
	vistagraficapadrina.getTextFieldIdPersona().setText(Integer.toString(apa.getPadrinos().getIdpersona()));
	vistagraficapadrina.getTextFieldTelefono().setText(apa.getPadrinos().getTelefono());
	vistagraficapadrina.getTextFieldIdPersona().setText(Integer.toString(apa.getPadrinos().getIdpersona()));
	vistagraficapadrina.getTextFieldIdPerro().setText(Integer.toString(apa.getPerros().getIdperro()));
	vistagraficapadrina.getBotonFecha().setDate(apa.getFechaapa());
    }

    private void cambioCBPadrinos() {
	int id;
	Iterator it;
	Padrinos padrino;
	String idCombo = (String) vistagraficapadrina.getComboLeerPadrinos().getSelectedItem();
	stokenizer = new StringTokenizer(idCombo, " - ");
	id = Integer.parseInt(stokenizer.nextToken());
	padrinos = imodelo.readPadrino();
	it = padrinos.iterator();
	while (it.hasNext()) {
	    padrino = (Padrinos) it.next();
	    if (padrino.getIdpersona() == id) {
		vistagraficapadrina.getTextFieldIdPersona().setText(Integer.toString(padrino.getIdpersona()));
		vistagraficapadrina.getTextFieldNombre().setText(padrino.getNombrepersona());
		vistagraficapadrina.getTextFieldTelefono().setText(padrino.getTelefono());
	    }
	}
    }

    private void cambioCBPerros() {
	int id;
	Iterator it;
	Perros perro;
	String idCombo = (String) vistagraficapadrina.getComboLeerPerros().getSelectedItem();
	stokenizer = new StringTokenizer(idCombo, " - ");
	id = Integer.parseInt(stokenizer.nextToken());
	perros = imodelo.readPerro();
	it = perros.iterator();
	while (it.hasNext()) {
	    perro = (Perros) it.next();
	    if (perro.getIdperro() == id) {
		vistagraficapadrina.getTextFieldIdPerro().setText(Integer.toString(perro.getIdperro()));
		vistagraficapadrina.getTextFieldMascota().setText(perro.getNombreperro());
		vistagraficapadrina.getTextFieldNChip().setText(Long.toString(perro.getNchip()));
	    }
	}
    }

    @Override
    public void mousePressed(MouseEvent e) {
	//no hace nada
    }

    @Override
    public void mouseReleased(MouseEvent e) {
	//no hace nada
    }

    @Override
    public void mouseEntered(MouseEvent e) {
	//no hace nada
    }

    @Override
    public void mouseExited(MouseEvent e) {
	//no hace nada
    }
    
    @Override
    public void focusGained(FocusEvent e) {
	//no hace nada
    }

    @Override
    public void focusLost(FocusEvent e) {
	//no hace nada
    }
}
