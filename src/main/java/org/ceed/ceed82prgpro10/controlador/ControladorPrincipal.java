package org.ceed.ceed82prgpro10.controlador;
import org.ceed.ceed82prgpro10.vista.VistaGrafica;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPadrino;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPerro;
import org.ceed.ceed82prgpro10.vista.VistaGraficaApadrina;
import org.ceed.ceed82prgpro10.vista.VistaGraficaAcercaDe;
import org.ceed.ceed82prgpro10.modelo.IModelo;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Desktop;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.InputMismatchException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;
import org.ceed.ceed82prgpro10.vista.Funciones;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPadrinoPerro;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPadTab;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPerTab;




/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorPrincipal implements ActionListener {
    VistaGrafica vistagrafica = new VistaGrafica();
    IModelo imodelo;
    
  public ControladorPrincipal (IModelo m, VistaGrafica v) throws InputMismatchException {
//    vistagrafica = v;
    imodelo = m;
    vistagrafica.menuGrafico();
//     Vigilamos los eventos sobre los botones que llevarán a distintas partes del menú
    vistagrafica.setLocationRelativeTo(null);
    vistagrafica.getBotonPadrino().addActionListener(this);
    vistagrafica.getBotonPerro().addActionListener(this);
    vistagrafica.getBotonApadrinamiento().addActionListener(this);
    vistagrafica.getBotonBdInst().addActionListener(this);
    vistagrafica.getBotonBdTab().addActionListener(this);
    vistagrafica.getBotonBdInsert().addActionListener(this);
    vistagrafica.getBotonDocuPDF().addActionListener(this);
    vistagrafica.getBotonDocuOnl().addActionListener(this);
    vistagrafica.getBotonSalir().addActionListener(this);
    vistagrafica.getBotonAcercade().addActionListener(this);  
    vistagrafica.getBotonPadPer().addActionListener(this);  
    vistagrafica.getBotonPerPad().addActionListener(this);
    vistagrafica.getBotonPadTab().addActionListener(this);
    vistagrafica.getBotonPerTab().addActionListener(this);  
    }
  
  @Override
  public void actionPerformed (ActionEvent event) { // Metodos implementados por ActionListener que vigila los botones
      if (vistagrafica.getBotonPadrino() == event.getSource()) {
          VistaGraficaPadrino menuAdmPadrino = VistaGraficaPadrino.getInstancia();
          if (!menuAdmPadrino.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmPadrino);
               menuAdmPadrino.setVisible(true);
          }
          try {
              menuAdmPadrino.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPadrino cp = new ControladorPadrino (imodelo, menuAdmPadrino );
      }
      else if (vistagrafica.getBotonPerro() == event.getSource()) {
          VistaGraficaPerro menuAdmPerro = VistaGraficaPerro.getInstancia();
          if (!menuAdmPerro.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmPerro);
               menuAdmPerro.setVisible(true);
          }
          try {
              menuAdmPerro.setMaximum(true);
              menuAdmPerro.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPerro cp = new ControladorPerro (imodelo, menuAdmPerro );

      }
      else if (vistagrafica.getBotonApadrinamiento() == event.getSource()) {
          VistaGraficaApadrina menuAdmApa = VistaGraficaApadrina.getInstancia();
          if (!menuAdmApa.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmApa);
               menuAdmApa.setVisible(true);
          }
          try {
              menuAdmApa.setMaximum(true);
              menuAdmApa.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorApadrina cp = new ControladorApadrina (imodelo, menuAdmApa);
      }
      
      else if (vistagrafica.getBotonPadPer() == event.getSource()) {
          VistaGraficaPadrinoPerro menuAdmApa = VistaGraficaPadrinoPerro.getInstancia();
          if (!menuAdmApa.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmApa);
               menuAdmApa.setVisible(true);
//               menuAdmApa.setTitle("Relación Padrinos - Perros");
          }
          try {
              menuAdmApa.setMaximum(true);
              menuAdmApa.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPadPer cp = new ControladorPadPer (imodelo, menuAdmApa, vistagrafica, 1);
      }
      
      else if (vistagrafica.getBotonPerPad() == event.getSource()) {
          VistaGraficaPadrinoPerro menuAdmApa = VistaGraficaPadrinoPerro.getInstancia();
          if (!menuAdmApa.isVisible()) {
               vistagrafica.getEscritorio().add(menuAdmApa);
               menuAdmApa.setVisible(true);
//               menuAdmApa.setTitle("Relación Perros - Padrinos");
          }
          try {
              menuAdmApa.setMaximum(true);
              menuAdmApa.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPadPer cp = new ControladorPadPer (imodelo, menuAdmApa, vistagrafica, 0);
      }
      
      else if (vistagrafica.getBotonAcercade() == event.getSource()) {
          VistaGraficaAcercaDe vade = VistaGraficaAcercaDe.getInstancia();
          vade.pack();
          if (!vade.isVisible()) {
               vistagrafica.getEscritorio().add(vade);
               vade.setVisible(true);
          }
          try {
              vade.setMaximum(true);
              vade.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorAcercaDe cp = new ControladorAcercaDe (vade );
          
      }
      else if (vistagrafica.getBotonBdInst() == event.getSource()) {
          imodelo.instalarDb();
      }
      else if (vistagrafica.getBotonBdTab() == event.getSource()) {
      }
      else if (vistagrafica.getBotonBdInsert() == event.getSource()) {
           imodelo.desinstalarDb();
      }
      else if (vistagrafica.getBotonPadTab() == event.getSource()) {
	    JInternalFrame jiframe = new JInternalFrame("Tabla maestra de Padrinos");
	    VistaGraficaPadTab jpanel = new VistaGraficaPadTab();
	    jiframe.add(jpanel);
	    vistagrafica.getEscritorio().add(jiframe);
	    jiframe.setVisible(true);
	    jiframe.setClosable(true);
	    jiframe.setIconifiable(true);
	    jiframe.setMaximizable(true);
	    try {
		jiframe.setMaximum(true);
		jiframe.setSelected(true);
	    } catch (PropertyVetoException localPropertyVetoException) {
	    }
      }
      else if (vistagrafica.getBotonPerTab() == event.getSource()) {
	    JInternalFrame jiframe = new JInternalFrame("Tabla maestra de Perros");
	    VistaGraficaPerTab jpanel = new VistaGraficaPerTab();
	    jiframe.add(jpanel);
	    vistagrafica.getEscritorio().add(jiframe);
	    jiframe.setVisible(true);
	    jiframe.setClosable(true);
	    jiframe.setIconifiable(true);
	    jiframe.setMaximizable(true);
	    try {
		jiframe.setMaximum(true);
		jiframe.setSelected(true);
	    } catch (PropertyVetoException localPropertyVetoException) {
	    }
      }
      else if (vistagrafica.getBotonDocuOnl() == event.getSource()) {
        String url = "https://docs.google.com/document/d/1MK6IUu3PphsD_4RWTs3LKS93c7YlQ4VOfGD4eRKKWqE/edit?ts=5624d84f#";
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.getDesktop().browse(new URI(url));
            } catch (URISyntaxException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
      }
      else if (vistagrafica.getBotonDocuPDF()== event.getSource()){
           try
	    {
	      String ruta = "src/main/java/org/ceed/documentacion/documentacion.pdf";
	      File pdf = new File(ruta);
	      Desktop.getDesktop().open(pdf);
	    }
	    catch (IOException ex)
	    {
	      Funciones.mensaje(null, "No se ha podido abrir el PDF.", "Error", 0);
	    }
	      }
      else if (vistagrafica.getBotonSalir() == event.getSource()) {
          System.exit(0);
      }
      
  }
   
 }
 
 

