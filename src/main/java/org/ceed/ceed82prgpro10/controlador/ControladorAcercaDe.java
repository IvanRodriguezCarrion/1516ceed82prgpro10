package org.ceed.ceed82prgpro10.controlador;
import javax.swing.WindowConstants;
/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

import org.ceed.ceed82prgpro10.vista.VistaGraficaAcercaDe;
import org.ceed.ceed82prgpro10.modelo.IModelo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.InputMismatchException;


public class ControladorAcercaDe implements ActionListener {
   VistaGraficaAcercaDe vistagraficaacercade;
   
   
   public ControladorAcercaDe (VistaGraficaAcercaDe v) throws InputMismatchException {
    vistagraficaacercade = v;
    vistagraficaacercade.getBotonSalir().addActionListener(this);

    }
   
   public void actionPerformed (ActionEvent event) { // Metodos implementados por ActionListener que vigila los botones
      if (vistagraficaacercade.getBotonSalir() == event.getSource()) {
          vistagraficaacercade.dispose();
      }
   }
   
   
}