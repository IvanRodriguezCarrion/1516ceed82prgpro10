/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceed.ceed82prgpro10.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceed.ceed82prgpro10.modelo.IModelo;
import org.ceed.ceed82prgpro10.modelo.ModeloDb4o;
import org.ceed.ceed82prgpro10.modelo.ModeloFichero;
import org.ceed.ceed82prgpro10.modelo.ModeloHibernate;
import org.ceed.ceed82prgpro10.modelo.ModeloMysql;
import org.ceed.ceed82prgpro10.vista.VistaGraficaLogin;
import org.ceed.ceed82prgpro10.modelo.Usuario;
import org.ceed.ceed82prgpro10.vista.Funciones;
import org.ceed.ceed82prgpro10.vista.VistaGrafica;
/**
 *
 * @author BlackBana
 */
public class ControladorLogin implements ActionListener {
	IModelo modelo;
	VistaGraficaLogin vgl;
    public ControladorLogin(IModelo m, VistaGraficaLogin v) {
	modelo = m;
	vgl = v;
	vgl.setLocationRelativeTo(null);
	vgl.setVisible(true);
	vgl.getBotonEntrar().addActionListener(this);
	vgl.getBotonSalir().addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
	if (vgl.getBotonEntrar() == e.getSource()) {
	    try {
		verificacion();
	    } catch (IOException ex) {
		Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
      else if (vgl.getBotonSalir() == e.getSource()) {
	vgl.dispose();
	}
    }
    
    private boolean comprobarAcceso() {
	Usuario us = new Usuario();
	String usuario = vgl.getTextFieldUsuario().getText();
	String contra = vgl.getPasswordFieldPass().getText();
	boolean bi = usuario.equals(us.getUsuario()) && contra.equals(us.getPassword());
	return bi;
    }
    
    private void verificacion() throws IOException {
	boolean correcto = comprobarAcceso();
	if (!correcto) {
		Funciones.mensaje(null, "El usuario y/o contraseña introducidos no es válido.", "ERROR EN LAS CREDENCIALES", 1);
	} else {
		modeloSeleccionado();
		VistaGrafica vg = null;
		ControladorPrincipal c = new ControladorPrincipal(modelo, vg);
	}
    }
    
    private void modeloSeleccionado(){
	String mod = (String) vgl.getComboModelo().getSelectedItem();
	    switch (mod) {
		case "Ficheros":
		    modelo = new ModeloFichero();
		    break;
		case "MySQL":
		    modelo = new ModeloMysql();
		    break;
		case "Db4O":
		    modelo = new ModeloDb4o();
		    break;
		case "Hibernate":
		    modelo = new ModeloHibernate();
		    break;
	    }
    }
}
