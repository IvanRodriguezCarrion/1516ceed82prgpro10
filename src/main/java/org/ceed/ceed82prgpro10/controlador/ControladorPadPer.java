/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceed.ceed82prgpro10.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.table.DefaultTableModel;
import org.ceed.ceed82prgpro10.modelo.Apadrina;
import org.ceed.ceed82prgpro10.modelo.IModelo;
import org.ceed.ceed82prgpro10.modelo.Padrinos;
import org.ceed.ceed82prgpro10.modelo.Perros;
import org.ceed.ceed82prgpro10.vista.VistaGrafica;
import org.ceed.ceed82prgpro10.vista.VistaGraficaApadrina;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPadrino;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPadrinoPerro;
import org.ceed.ceed82prgpro10.vista.VistaGraficaPerro;

/**
 *
 * @author BlackBana
 */
public class ControladorPadPer implements MouseListener  {
    VistaGrafica vg;
    VistaGraficaPadrinoPerro vpp;
    IModelo modelo;
    ArrayList padrinos;
    ArrayList perros;
    ArrayList apadrinas;

    public ControladorPadPer(IModelo m, VistaGraficaPadrinoPerro v, VistaGrafica vga, int posicion ) {
        vg = vga;
        vpp = v;
        modelo = m;
        llenarTablas();
        vpp.getBotonIrPad().addMouseListener(this);
        vpp.getBotonIrPer().addMouseListener(this);
        vpp.getBotonIrApa().addMouseListener(this);
        vpp.getBotonRecargar().addMouseListener(this);
        vpp.getBotonSalir().addMouseListener(this);
        vpp.getSliderPadPer().addMouseListener(this);
        vpp.getTDominante().addMouseListener(this);
        
        if (posicion == 0) {
            dominantePerro();
            tablaPerros(perros);
        } else if (posicion == 1) {
            dominantePadrino();
            tablaPadrinos(padrinos);
        }
    }
       
    private void dominantePadrino() {
        vpp.getPanelDominante().setBorder(javax.swing.BorderFactory.createTitledBorder("Padrinos"));
        vpp.getPanelRecesivo().setBorder(javax.swing.BorderFactory.createTitledBorder("Perros"));
        vpp.setTitle("Relación Padrinos - Perros");
        vpp.getSliderPadPer().setValue(1);   
    }
    
     private void dominantePerro() {
        vpp.getPanelDominante().setBorder(javax.swing.BorderFactory.createTitledBorder("Perros"));
        vpp.getPanelRecesivo().setBorder(javax.swing.BorderFactory.createTitledBorder("Padrinos"));
        vpp.setTitle("Relación Perros - Padrinos");
        vpp.getSliderPadPer().setValue(0);   
    }
    
    
    private void tablaPadrinos(ArrayList padrinos) {
        if (!padrinos.isEmpty()) {
            vpp.getTDominante().setVisible(true);
            String[] padrino = { "Identificador", "Nombre", "Teléfono", "Email" };
      
            String[] tablas = new String[padrino.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, padrino);
      
            Iterator it = padrinos.iterator();
            while (it.hasNext()) {
                Padrinos p = (Padrinos)it.next();
                tablas[0] = Integer.toString(p.getIdpersona());
                tablas[1] = p.getNombrepersona();
                tablas[2] = p.getTelefono();
                tablas[3] = p.getEmail();
                dtm.addRow(tablas);
            }
                vpp.getTDominante().setModel(dtm);
            
        } else {
            vpp.getTDominante().setVisible(false);
        }
    }
    
    private void tablaPerros(ArrayList perros) {
        if (!perros.isEmpty()) {
            vpp.getTDominante().setVisible(true);
            String[] perro = { "Identificador", "Nombre", "Número de Chip", "Raza" };
      
            String[] tablas = new String[perro.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, perro);
      
            Iterator it = perros.iterator();
            while (it.hasNext()) {
                Perros p = (Perros)it.next();
                tablas[0] = Integer.toString(p.getIdperro());
                tablas[1] = p.getNombreperro();
                tablas[2] = Long.toString(p.getNchip());
                tablas[3] = p.getRaza();
                dtm.addRow(tablas);
            }
                vpp.getTDominante().setModel(dtm);
         
        } else {
            vpp.getTDominante().setVisible(false);
        }
    }
    
    private void tablaRecuperarPadrinos(ArrayList apadrinas, Perros pe) {
        if (!padrinos.isEmpty()) {
            vpp.getTRecesivo().setVisible(true);
            String[] padrino = { "Identificador", "Nombre", "Teléfono", "Email" };
      
            String[] tablas = new String[padrino.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, padrino);
      
            Iterator it = apadrinas.iterator();
            while (it.hasNext()) {
                Apadrina p = (Apadrina)it.next();
                if (p.getPerros().getIdperro() == pe.getIdperro()) {
                tablas[0] = Integer.toString(p.getPadrinos().getIdpersona());
                tablas[1] = p.getPadrinos().getNombrepersona();
                tablas[2] = p.getPadrinos().getTelefono();
                tablas[3] = p.getPadrinos().getEmail();
                dtm.addRow(tablas);
                }
            }
            vpp.getTRecesivo().setModel(dtm);
        } else {
            vpp.getTRecesivo().setVisible(false);
        }
    }
    
    private void tablaRecuperarPerros(ArrayList apadrinas, Padrinos pa) {
        if (!perros.isEmpty()) {
            vpp.getTRecesivo().setVisible(true);
            String[] perro = { "Identificador", "Nombre", "Número de Chip", "Raza" };
      
            String[] tablas = new String[perro.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, perro);
      
            Iterator it = apadrinas.iterator();
            while (it.hasNext()) {
                Apadrina p = (Apadrina)it.next();
                if (p.getPadrinos().getIdpersona() == pa.getIdpersona()) {
                tablas[0] = Integer.toString(p.getPerros().getIdperro());
                tablas[1] = p.getPerros().getNombreperro();
                tablas[2] = Long.toString(p.getPerros().getNchip());
                tablas[3] = p.getPerros().getRaza();
                dtm.addRow(tablas);
                }
            }
            vpp.getTRecesivo().setModel(dtm);
        } else {
            vpp.getTRecesivo().setVisible(false);
        }
    }
    
    private void llenarTablas() {
        padrinos = modelo.readPadrino();
        perros = modelo.readPerro();
        apadrinas = modelo.readApadrina();
        if (vpp.getSliderPadPer().getValue() == 0){
            tablaPerros(perros);
        } else if (vpp.getSliderPadPer().getValue() == 1) {
            tablaPadrinos(padrinos);
        } 
    }
    
    private void devolverPerro(MouseEvent e) {
        Padrinos padrino = null;
        apadrinas = modelo.readApadrina();
        int fila = vpp.getTDominante().rowAtPoint(e.getPoint());
        if (fila != -1) {
            int id = Integer.parseInt((String)vpp.getTDominante().getValueAt(fila, 0));
            int i = 0;
            while (i < padrinos.size()) {
                padrino = (Padrinos)padrinos.get(i);
                if (padrino.getIdpersona() == id) {
                    break;
                }
                i++;
            }
        }
        if (padrino != null) {    
            tablaRecuperarPerros(apadrinas, padrino);
        }
    }
    
    private void devolverPadrino(MouseEvent e) {
        Perros perro = null;
        apadrinas = modelo.readApadrina();
        int fila = vpp.getTDominante().rowAtPoint(e.getPoint());
        if (fila != -1) {
            int id = Integer.parseInt((String)vpp.getTDominante().getValueAt(fila, 0));
            int i = 0;
            while (i < perros.size()) {
                perro = (Perros)perros.get(i);
                if (perro.getIdperro() == id) {
                    break;
                }
                i++;
            }
        }
        if (perro != null) {    
            tablaRecuperarPadrinos(apadrinas, perro);
        }
    }
    
    

    @Override
    public void mouseClicked(MouseEvent e) {
        if (vpp.getSliderPadPer() == e.getSource()) {
            cambioEstadoSlider();
      }
        else if (vpp.getTDominante() == e.getSource()) {
            if (vpp.getSliderPadPer().getValue() == 0){
               vpp.getTRecesivo().setVisible(true);
               devolverPadrino(e);
            } else if (vpp.getSliderPadPer().getValue() == 1) {
               vpp.getTRecesivo().setVisible(true);
               devolverPerro(e);    
            }
        }
        else if (vpp.getBotonSalir() == e.getSource()) {
            vpp.dispose();
        }
        else if (vpp.getBotonRecargar() == e.getSource()) {
            llenarTablas();
        }
        else if (vpp.getBotonIrPad() == e.getSource()) {
            VistaGraficaPadrino menuAdmPadrino = VistaGraficaPadrino.getInstancia();
          if (!menuAdmPadrino.isVisible()) {
               vg.getEscritorio().add(menuAdmPadrino);
               menuAdmPadrino.setVisible(true);
          }
          try {
              menuAdmPadrino.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPadrino cp = new ControladorPadrino (modelo, menuAdmPadrino );
        }
        else if (vpp.getBotonIrPer() == e.getSource()) {
             VistaGraficaPerro menuAdmPerro = VistaGraficaPerro.getInstancia();
          if (!menuAdmPerro.isVisible()) {
               vg.getEscritorio().add(menuAdmPerro);
               menuAdmPerro.setVisible(true);
          }
          try {
              menuAdmPerro.setMaximum(true);
              menuAdmPerro.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorPerro cp = new ControladorPerro (modelo, menuAdmPerro );

        }
        else if (vpp.getBotonIrApa() == e.getSource()) {
            VistaGraficaApadrina menuAdmApa = VistaGraficaApadrina.getInstancia();
          if (!menuAdmApa.isVisible()) {
               vg.getEscritorio().add(menuAdmApa);
               menuAdmApa.setVisible(true);
          }
          try {
              menuAdmApa.setMaximum(true);
              menuAdmApa.setSelected(true);
          } catch (PropertyVetoException ex) {
              ex.printStackTrace();
          }
          ControladorApadrina cp = new ControladorApadrina (modelo, menuAdmApa);
        }
    }

    public void cambioEstadoSlider() {
        if (vpp.getSliderPadPer().getValue() == 0){
                dominantePerro();
                tablaPerros(perros);
                 vpp.getTRecesivo().setVisible(false);
            } else if (vpp.getSliderPadPer().getValue() == 1) {
                 dominantePadrino();
                 tablaPadrinos(padrinos);
                 vpp.getTRecesivo().setVisible(false);
            }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
    
}
