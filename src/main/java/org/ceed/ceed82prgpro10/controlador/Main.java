package org.ceed.ceed82prgpro10.controlador;
import org.ceed.ceed82prgpro10.vista.VistaGrafica;
import org.ceed.ceed82prgpro10.modelo.IModelo;
import org.ceed.ceed82prgpro10.modelo.ModeloMysql;
import java.util.InputMismatchException;
import org.ceed.ceed82prgpro10.modelo.ModeloDb4o;
import org.ceed.ceed82prgpro10.modelo.ModeloFichero;
import org.ceed.ceed82prgpro10.modelo.ModeloHibernate;
import org.ceed.ceed82prgpro10.vista.VistaGraficaLogin;



/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Main {
        
  public static void main (String[] args) throws InputMismatchException {
   
   /**
    * La clase Main se encargará de controlar la ejecución
    * de los procesos del programa importando las clases
    * de los paquetes modelo y vista.
   */ 
    
        IModelo modelo = new ModeloDb4o();
	VistaGraficaLogin vista = new VistaGraficaLogin();
	ControladorLogin cp = new ControladorLogin (modelo, vista);

    }
}  